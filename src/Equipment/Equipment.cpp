
#include "Equipment.h"

Equipment::Equipment(EquipmentData equipmentData) :
	textureName(equipmentData.fileName),
	name(equipmentData.equipmentName),
	equipmentType(equipmentData.equipmentType)
{
	this->setTexture();
}

Equipment::~Equipment()
{
}

const sf::Texture* Equipment::getTexture() const
{
	return &(this->texture);
}

const unsigned short Equipment::getEquipmentName() const
{
	return this->name;
}

const EquipmentType Equipment::getEquipmentType() const
{
	return this->equipmentType;
}

void Equipment::setTexture()
{
	this->texture.loadFromFile("Ressources/Images/Sprites/Equipment/" + this->textureName + ".png");
	this->texture.setSmooth(true);
}