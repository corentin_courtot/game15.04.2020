#pragma once

#include "EntityEnum.h"
#include "EquipmentData.h"

class Equipment
{
protected:
	sf::Texture texture;
	std::string textureName;

	unsigned short name;
	EquipmentType equipmentType;

	void setTexture();

public:
	// Constructor/Destructor
	Equipment(EquipmentData equipmentData);
	~Equipment();

	//Accessor
	const sf::Texture* getTexture() const;
	const unsigned short getEquipmentName() const;
	const EquipmentType getEquipmentType() const;
};
