#pragma once

#include "Equipment.h"
#include "MeleeWeapon.h"
#include "RangeWeapon.h"

class EquipmentComponent
{
private:
	std::vector<Equipment*> equipment;
	Weapon* weapon;

public:
	EquipmentComponent();
	~EquipmentComponent();

	std::vector<Equipment*>& getEquipment();
	Weapon* getWeapon();

	const bool canWear(EquipmentType type);
	bool addEquipment(EquipmentData equipmentData);
	bool addWeapon(unsigned short weaponName, sf::Sprite& referenceSprite);
};
