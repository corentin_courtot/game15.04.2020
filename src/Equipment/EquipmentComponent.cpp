
#include "EquipmentComponent.h"

EquipmentComponent::EquipmentComponent()
{
	this->equipment.resize(NB_EQUIPMENT - 2);
	this->weapon = NULL;
}

EquipmentComponent::~EquipmentComponent()
{
	for (size_t i = 0; i < this->equipment.size(); i++)
	{
		delete this->equipment[i];
		this->equipment[i] = NULL;
	}
	if (this->weapon)
	{
		delete this->weapon;
		this->weapon = NULL;
	}
}

std::vector<Equipment*>& EquipmentComponent::getEquipment()
{
	return this->equipment;
}

Weapon* EquipmentComponent::getWeapon()
{
	return this->weapon;
}

const bool EquipmentComponent::canWear(EquipmentType type)
{
	return true;
}

bool EquipmentComponent::addEquipment(EquipmentData equipmentData)
{
	if (canWear(equipmentData.equipmentType))
	{
		equipment[equipmentData.equipmentType - 2] = new Equipment(equipmentData);

		return true;
	}

	return false;
}

bool EquipmentComponent::addWeapon(unsigned short weaponName, sf::Sprite& referenceSprite)
{
	if ((EquipmentName)weaponName == bow)
		this->weapon = new RangeWeapon(Bow, referenceSprite);
	else if ((EquipmentName)weaponName == woodenSword)
		this->weapon = new MeleeWeapon(WoodenSword, referenceSprite);

	return true;
}