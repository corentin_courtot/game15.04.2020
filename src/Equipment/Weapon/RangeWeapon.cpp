#include "RangeWeapon.h"

RangeWeapon::RangeWeapon(
	RangeWeaponData rangeWeaponData,
	sf::Sprite& referenceSprite) :
	Weapon(rangeWeaponData.weaponData, referenceSprite),
	projectileData(rangeWeaponData.projectileData)
{
}

RangeWeapon::~RangeWeapon()
{
}

void RangeWeapon::hit(std::vector<Entity*>& entities, sf::Vector2f characterSpeed)
{
	sf::Vector2f dir(cos(this->targetAngleInRadian), sin(this->targetAngleInRadian));

	entities.push_back(new Projectile(
		this->inUseSprite.getPosition() + 35.f * dir,
		this->projectileData,
		this->projectileData.speed * dir,
		freeId(entities)));
}

void RangeWeapon::resetTarget()
{
	Weapon::resetTarget();
}

void RangeWeapon::setTarget(const sf::Vector2f& target)
{
	Weapon::setTarget(target);
	weaponState = RESTING;
	stagingTime = 0.f;
}