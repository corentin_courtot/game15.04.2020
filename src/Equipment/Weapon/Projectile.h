#pragma once

#include "DamageStatistic.h"
#include "HitboxEntity.h"

class Projectile :
	public HitboxEntity
{
private:
	sf::Texture texture;

	float angle;
	sf::Vector2f velocity;

	float timeLeft;

	DamageStatistic damageStatistic;
	//float damageLocality; // coefficient decrivant ...

	void initTexture(ProjectileData projectileData);

public:
	Projectile(sf::Vector2f pos,
		ProjectileData projectileData,
		sf::Vector2f velocity,
		int id = 0,
		unsigned short belongId = 0);
	Projectile(const DataTransmitted& data);
	~Projectile();

	virtual void getHit(std::vector<Entity*>& entities, DamageStatistic& damageStatistic);

	//* Network
	virtual void toDataTransmitted(DataTransmitted& data) const;
	virtual void updateAccordingToServerData(DataTransmitted& data);

	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
	//render();
};