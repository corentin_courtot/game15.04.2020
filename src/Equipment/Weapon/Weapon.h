#pragma once

#include "Entity.h"
#include "Equipment.h"
#include "StanceComponent.h"

enum WeaponState
{
	RESTING,
	ARMING,
	HITING,
	RELOADING
};

class Weapon : public Equipment
{
private:
protected:
	sf::Sprite& referenceSprite;

	sf::Vector2f* target;
	float targetAngleInRadian;

	WeaponState weaponState;

	std::vector<std::string> keys; // key of the stances. cf stanceComponent
	StanceComponent* stanceComponent;

	sf::Sprite inUseSprite;

	float stagingTime; // gere le passage d'un etat à un autre

	float armingTime; // valeurs pseudo constantes, propre à chaque weapon
	float reloadingTime;

	float offset; // offset dans la direction de la target

public:
	Weapon(WeaponData weaponData, sf::Sprite& referenceSprite);
	virtual ~Weapon();

	virtual sf::Vector2f* getTarget();
	virtual const float getAngle() const;

	virtual void resetTarget();
	virtual void setTarget(const sf::Vector2f& target);

	virtual void hit(std::vector<Entity*>& entities, sf::Vector2f characterSpeed) = 0; // gere les evenements lorsque qu'on tape/tire avec l'arme

	virtual void updateState(std::vector<Entity*>& entities, sf::Vector2f characterSpeed);

	virtual void update(const float& dt, std::vector<Entity*>& entities, sf::Vector2f characterSpeed);
	virtual void render(sf::RenderTarget& target) const;
};
