#include "Weapon.h"

Weapon::Weapon(WeaponData weaponData, sf::Sprite& referenceSprite) :
	Equipment(weaponData.equipmentData),
	referenceSprite(referenceSprite),
	target(NULL),
	targetAngleInRadian(0.f),
	weaponState(RESTING),
	stagingTime(0.f),
	armingTime(weaponData.armingTime),
	reloadingTime(weaponData.reloadingTime),
	offset(weaponData.offset)
{
	this->keys.resize(0);
	this->keys.push_back("arming");
	this->keys.push_back("hiting");
	this->keys.push_back("reloading");

	this->stanceComponent = new StanceComponent(this->inUseSprite);
	this->stanceComponent->setupStance(
		"Ressources/Images/Sprites/Equipment/inUse" + this->textureName + ".png",
		keys,
		sf::Vector2f(90.f, 60.f),
		30.f);

	this->stanceComponent->play("arming");

	this->inUseSprite.setOrigin(sf::Vector2f(0.f, 30.f));

	this->armingTime = 0.5f;
	this->reloadingTime = 1.f;
}

Weapon::~Weapon()
{
	if (this->target)
	{
		delete this->target;
		this->target = NULL;
	}
}

sf::Vector2f* Weapon::getTarget()
{
	return this->target;
}

const float Weapon::getAngle() const
{
	return this->targetAngleInRadian;
}

void Weapon::resetTarget()
{
	if (this->target)
	{
		delete this->target;
		this->target = NULL;
	}
}

void Weapon::setTarget(const sf::Vector2f& target)
{
	resetTarget();
	this->target = new sf::Vector2f(target);
}

void Weapon::updateState(std::vector<Entity*>& entities, sf::Vector2f characterSpeed)
{
	if (stagingTime <= 0.f && this->target)
	{ // update rangeWeapon stage
		switch (weaponState)
		{
			case RESTING:
				this->stagingTime = this->armingTime;
				this->weaponState = ARMING;
				this->stanceComponent->play("arming");
				break;

			case ARMING:
				this->hit(entities, characterSpeed);
				this->stagingTime = 0.07f; // time during which the hitting stance will appear on screen
				this->weaponState = HITING;
				this->stanceComponent->play("hiting");
				break;

			case HITING:
				this->stagingTime = this->reloadingTime;
				this->weaponState = RELOADING;
				this->stanceComponent->play("reloading");
				break;

			case RELOADING:
				this->stagingTime = 0.f;
				this->weaponState = RESTING;
				break;

			default:
				break;
		}
	}
}

void Weapon::update(const float& dt, std::vector<Entity*>& entities, sf::Vector2f characterSpeed)
{
	// updating weapon sprite position
	if (this->target)
	{
		// mise a jour de l'angle de l'arme
		sf::Vector2f v = *this->target - this->inUseSprite.getPosition();

		if (v.x > 0)
			this->targetAngleInRadian = atan(v.y / v.x);
		else
			this->targetAngleInRadian = atan(v.y / v.x) + PI;

		// switching orientation occording to target
		if (this->inUseSprite.getScale().y > 0 && (targetAngleInRadian > PI * 0.5f || targetAngleInRadian < -PI * 0.5f))
			this->inUseSprite.setScale(1.f, -1.f);
		else if (this->inUseSprite.getScale().y < 0 && (targetAngleInRadian < PI * 0.5f && targetAngleInRadian > -PI * 0.5f))
			this->inUseSprite.setScale(1.f, 1.f);

		// positionnement de l'arme par rapport au sprite auquel elle se rapporte
		sf::Vector2f offset(cos(this->targetAngleInRadian) * this->offset, -.5f * tileSize.y + sin(this->targetAngleInRadian) * this->offset * 2);

		this->inUseSprite.setPosition(this->referenceSprite.getPosition() + offset);

		this->inUseSprite.setRotation(this->targetAngleInRadian * 180.f / PI);
	}

	this->stagingTime -= dt;

	// resetting stages if is moving or no target
	if (characterSpeed != sf::Vector2f(0.f, 0.f) || !this->target)
	{
		this->stagingTime = 0.f;
		this->weaponState = RESTING;
	}

	this->updateState(entities, characterSpeed);
}

void Weapon::render(sf::RenderTarget& target) const
{
	if (this->target)
	{
		target.draw(this->inUseSprite);
	}
}