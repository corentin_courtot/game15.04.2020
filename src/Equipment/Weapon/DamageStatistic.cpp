#include "DamageStatistic.h"

DamageStatistic::DamageStatistic(float damage) :
	damageOnHit(damage)
{
}

DamageStatistic::~DamageStatistic()
{
}

/********************************************************
 * 					Accessors
 ******************************************************/
float DamageStatistic::getDamage() const
{
	return damageOnHit;
}
void DamageStatistic::setDamage(const float damage)
{
	this->damageOnHit = damage;
}