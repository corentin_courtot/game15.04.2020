#include "MeleeWeapon.h"

MeleeWeapon::MeleeWeapon(MeleeWeaponData meleeWeaponData,
	sf::Sprite& referenceSprite) :
	Weapon(meleeWeaponData.weaponData, referenceSprite),
	hitboxEffectData(meleeWeaponData.hitboxEffectData)
{
}

MeleeWeapon::~MeleeWeapon()
{
}

void MeleeWeapon::hit(std::vector<Entity*>& entities, sf::Vector2f characterSpeed)
{
	sf::Vector2f dir(cos(this->targetAngleInRadian), sin(this->targetAngleInRadian));

	entities.push_back(new HitboxEffect(
		this->inUseSprite.getPosition() + 23.f * dir,
		this->targetAngleInRadian,
		this->hitboxEffectData,
		freeId(entities)));
}

void MeleeWeapon::resetTarget()
{
	Weapon::resetTarget();
}

void MeleeWeapon::setTarget(const sf::Vector2f& target)
{
	Weapon::setTarget(target);
	weaponState = RESTING;
	stagingTime = 0.f;
}
