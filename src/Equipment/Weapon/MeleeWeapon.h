#pragma once

#include "HitboxEffect.h"
#include "Weapon.h"

class MeleeWeapon : public Weapon
{
private:
	HitboxEffectData hitboxEffectData;

public:
	MeleeWeapon(MeleeWeaponData meleeWeaponData,
		sf::Sprite& referenceSprite);
	~MeleeWeapon();

	virtual void hit(std::vector<Entity*>& entities, sf::Vector2f characterSpeed);

	virtual void resetTarget();
	virtual void setTarget(const sf::Vector2f& target);
};