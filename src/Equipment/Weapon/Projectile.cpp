#include "Projectile.h"

void Projectile::initTexture(ProjectileData projectileData)
{
	this->texture.loadFromFile("Ressources/Images/Sprites/Projectiles/" + projectileData.fileName + ".png");
	this->texture.setSmooth(true);

	this->sprite.setTexture(texture);

	//this->textureSheet = renderTexture.getTexture();
	this->textureSize = sf::Vector2f(30.f, 30.f);
}

// Constructor
Projectile::Projectile(sf::Vector2f pos,
	ProjectileData projectileData,
	sf::Vector2f velocity,
	int id,
	unsigned short belongId) :
	Entity(projectileData.entityType, id, belongId),
	HitboxEntity(projectileData.entityType, id, belongId),
	velocity(velocity),
	timeLeft(projectileData.timeLeft)
{
	this->angle = atan(this->velocity.y / this->velocity.x) * 180 / PI;
	if (this->velocity.x < 0)
		this->angle += 180.f;

	this->sprite.setOrigin(sf::Vector2f(15.f, 15.f));
	this->setPosition(pos);
	this->sprite.rotate(this->angle);

	this->initTexture(projectileData);

	this->createHitboxComponent(this->sprite, 0.f, 0.f, 12.f, 5.f, this->angle);

	this->damageStatistic.setDamage(projectileData.damage);
}

Projectile::Projectile(const DataTransmitted& data) :
	Entity((EntityType)data.objectType, data.id, data.belongId),
	HitboxEntity((EntityType)data.objectType, data.id, data.belongId),
	velocity(data.velocity),
	timeLeft(data.target.y)
{
	this->angle = atan(this->velocity.y / this->velocity.x) * 180 / PI;
	if (this->velocity.x < 0)
		this->angle += 180.f;

	this->sprite.setOrigin(sf::Vector2f(15.f, 15.f));
	this->setPosition(data.pos);
	this->sprite.rotate(this->angle);

	this->createHitboxComponent(this->sprite, 0.f, 0.f, 12.f, 5.f, this->angle);

	this->damageStatistic.setDamage(data.target.x);

	if (data.objectType == arrow)
	{
		initTexture(Arrow);
	}
}

Projectile::~Projectile()
{
}

void Projectile::getHit(std::vector<Entity*>& entities, DamageStatistic& damageStatistic)
{
	this->timeLeft = 0.f;
}

void Projectile::toDataTransmitted(DataTransmitted& data) const
{
	Entity::toDataTransmitted(data);
	data.velocity = this->velocity;
	data.target.x = this->damageStatistic.getDamage();
	data.target.y = this->timeLeft;
}

void Projectile::updateAccordingToServerData(DataTransmitted& data)
{
	Entity::updateAccordingToServerData(data);
	this->velocity = data.velocity;
	this->damageStatistic.setDamage(data.target.x);
	this->timeLeft = data.target.y;
}

bool Projectile::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{

	this->sprite.setPosition(this->sprite.getPosition() + this->velocity * dt);
	this->hitboxComponent->update(); // Replace la Hitbox a l'endroit ou se trouve effectivement le l'entite

	std::vector<Entity*> hitEntities = this->getEntitiesColliding(entities, mapEntities);

	if (hitEntities.size() > 0)
	// si le projectile est entré en collision avec une entite
	// on fait subir les degats du projectile aux sprites touchés
	{
		// application des effets du projectile aux entites touchees
		for (size_t i = 0; i < hitEntities.size(); ++i)
		{
			hitEntities[i]->getHit(entities, damageStatistic);
		}
		this->velocity = sf::Vector2f(0.f, 0.f);

		this->timeLeft = 0.f;
	}

	this->timeLeft -= dt;

	if (Entity::update(dt, entities, mapEntities, gameMap, visibilityMap) && this->timeLeft > 0)
	{
		return true;
	}

	return false;
}