#pragma once

#include "Projectile.h"
#include "Weapon.h"

class RangeWeapon : public Weapon
{
private:
	ProjectileData projectileData;

public:
	RangeWeapon(RangeWeaponData rangeWeaponData,
		sf::Sprite& referenceSprite);
	virtual ~RangeWeapon();

	virtual void hit(std::vector<Entity*>& entities, sf::Vector2f characterSpeed);

	virtual void resetTarget();
	virtual void setTarget(const sf::Vector2f& target);
};