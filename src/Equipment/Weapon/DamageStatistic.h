#pragma once

class DamageStatistic
{
private:
	float damageOnHit;

public:
	DamageStatistic(float damage = 0);
	~DamageStatistic();

	/*****************************************************
	 * 				Accessors
	 *****************************************************/
	float getDamage() const;
	void setDamage(const float damage);
};
