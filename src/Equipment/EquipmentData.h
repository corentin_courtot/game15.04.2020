#pragma once

#include "EntityData.h"

enum EquipmentType
{
	NONE = 0,
	WEAPON,
	TSHIRT,
	PANTS,
	//HAT,
	//JACKET,
	NB_EQUIPMENT
};

enum EquipmentName : unsigned int
{
	tShirtM1 = 0,
	tShirtF1,
	pant1,
	pant2,
	bow,
	woodenSword
};

const unsigned int nbEquipment = 4; // does not take weapon into account

struct EquipmentData
{
	EquipmentName equipmentName;
	std::string fileName;
	EquipmentType equipmentType;
};

const EquipmentData equipmentDict[nbEquipment] = { // data sous forme de tableau car plus opti pour la synchro serveur/client
	{ tShirtM1, "TShirtM1", TSHIRT },
	{ tShirtF1, "TShirtF1", TSHIRT },
	{ pant1, "Pant1", PANTS },
	{ pant2, "Pant2", PANTS }
};

struct WeaponData
{
	EquipmentData equipmentData;
	float armingTime;
	float reloadingTime;
	float offset; // offset dans la direction visee par l'arme
};

struct RangeWeaponData
{
	WeaponData weaponData;
	ProjectileData projectileData;
};

const RangeWeaponData Bow = {
	{
		// WeaponData struct
		{
			// EquipmentDataStruct
			bow,	// equipmentName;
			"Bow",	// fileName
			WEAPON, // EquipmentType
		},
		0.5f, // arming time
		1.f,  // reloading time
		0.f	  // offest
	},
	Arrow
};

struct MeleeWeaponData
{
	WeaponData weaponData;
	HitboxEffectData hitboxEffectData;
};

const MeleeWeaponData WoodenSword = {
	{
		// WeaponData struct
		{
			// EquipmentDataStruct
			woodenSword,   // equipmentName;
			"WoodenSword", // fileName
			WEAPON,		   // EquipmentType },
		},
		0.5f, // arming time
		1.f,  // reloading time
		10.f  // offset
	},
	MeleeSlashEffect
};