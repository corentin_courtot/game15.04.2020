#include "GeneralFunctions.h"

float dist(float a, float b)
{
	return sqrt(a * a + b * b);
}

// float dist(sf::Vector2f v)
// {
// 	return sqrt(v.x * v.x + v.y * v.y);
// }

// float squareDist(sf::Vector2f v)
// {
// 	return abs(v.x) + abs(v.y);
// }

sf::Vector2i vector2fToIndTile(const sf::Vector2f& v)
{
	return sf::Vector2i(floor(v.x / tileSize.x), floor(v.y / tileSize.y));
}

sf::Vector2f IndTileToVector2f(const sf::Vector2i& v)
{
	return sf::Vector2f((v.x + 0.5f) * tileSize.x, (v.y + 0.5f) * tileSize.y);
}

int pixelDistToTileDist(int d)
{
	return (int)(d / std::min(tileSize.x, tileSize.y)) + 1;
}

namespace myRand
{
void srand(int seed)
{
	state = seed;
}

int rand()
{
	int const a = 1103515245;
	int const c = 12345;
	state = a * state + c;
	return (state >> 16) & 0x7FFF;
}
}