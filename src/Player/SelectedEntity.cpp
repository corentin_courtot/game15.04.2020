
#include "SelectedEntity.h"

SelectedEntity::SelectedEntity()
{
	this->isEmpty = true;

	this->selectedEntityTexture.loadFromFile("Ressources/Images/Sprites/Divers/SelectedEntity.png");
	this->selectedEntityTexture.setSmooth(true);
	this->selectedEntitySprite.setTexture(selectedEntityTexture);
}

SelectedEntity::~SelectedEntity()
{
}

bool SelectedEntity::isSet()
{
	return !isEmpty;
}

void SelectedEntity::reset()
{
	int i = (int)this->selectedEntity.size() - 1;
	while (i >= 0)
	{
		selectedEntity[i] = NULL;
		selectedEntity.pop_back();
		i = (int)this->selectedEntity.size() - 1;
	}

	this->isEmpty = true;
}

void SelectedEntity::addEntity(Entity* entity)
{
	this->selectedEntity.push_back(entity);

	this->isEmpty = false;
}

void SelectedEntity::updateSelectedEntityInput(const float& dt, sf::Vector2f mousePosView)
{
	for (size_t i = 0; i < this->selectedEntity.size(); ++i)
	{
		if (this->selectedEntity[i])
		{
			// gestion on-clique movement
			{
				if (this->selectedEntity[i]->canMove() && sf::Mouse::isButtonPressed(sf::Mouse::Right))
					this->selectedEntity[i]->setMovementTarget(mousePosView);
			}

			// gestion Z,Q,S,D
			{
				sf::Vector2f target = this->selectedEntity[i]->getCoord();
				if (sf::Keyboard::isKeyPressed((sf::Keyboard::Z)))
					target.y -= (float)tileSize.y;

				if (sf::Keyboard::isKeyPressed((sf::Keyboard::S)))
					target.y += (float)tileSize.y;

				if (sf::Keyboard::isKeyPressed((sf::Keyboard::Q)))
					target.x -= (float)tileSize.x;

				if (sf::Keyboard::isKeyPressed((sf::Keyboard::D)))
					target.x += (float)tileSize.x;

				if (target != this->selectedEntity[i]->getCoord())
				// si une entree clavier a ete enregistree
				{
					this->selectedEntity[i]->setMovementTarget(target);
				}
			}

			// gestion on-clique shooting
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !sf::Keyboard::isKeyPressed((sf::Keyboard::LShift)))
			{
				this->selectedEntity[i]->setWeaponTarget(mousePosView);
			}
		}
	}
}

void SelectedEntity::render(sf::RenderTarget* target)
{
	for (size_t i = 0; i < this->selectedEntity.size(); ++i)
	{
		if (this->selectedEntity[i])
		{
			this->selectedEntitySprite.setScale(
				this->selectedEntity[i]->getTextureSize().x / sf::Vector2f(30.f, 30.f).x,
				this->selectedEntity[i]->getTextureSize().y / sf::Vector2f(30.f, 30.f).y);
			this->selectedEntitySprite.setPosition(
				this->selectedEntity[i]->getCoord() - this->selectedEntity[i]->getOrigin());

			target->draw(selectedEntitySprite);
		}
	}
}
