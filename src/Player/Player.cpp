#include "Player.h"

Player::Player(unsigned short id = 0) :
	id(id)
{
	std::cout << "(Player) initialized with id " << id << std::endl;
}

Player::~Player()
{
}

const int Player::getId() const
{
	return this->id;
}

void Player::updateSelectedEntity(const std::vector<Entity*>& entities, const sf::Vector2f& mousePosView)
{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && sf::Keyboard::isKeyPressed((sf::Keyboard::LShift)))
	{
		this->selection.reset();

		size_t i(0);
		while (i < entities.size())
		{
			if (entities[i]
				&& entities[i]->canMove()
				&& entities[i]->getBelongId() == this->id
				&& entities[i]->isClicked(mousePosView))
			{
				selection.addEntity(entities[i]);
			}
			++i;
		}
	}
}

void Player::updateSelectedEntityInput(const float& dt, const sf::Vector2f& mousePosView)
{
	this->selection.updateSelectedEntityInput(dt, mousePosView);
}

void Player::update()
{
}

void Player::render(sf::RenderTarget* target)
{
	this->selection.render(target);
}