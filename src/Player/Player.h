#pragma once

#include "SelectedEntity.h"

class Player
{
private:
	unsigned short id;

	SelectedEntity selection;

public:
	Player(unsigned short id);
	~Player();

	const int getId() const;

	void updateSelectedEntity(const std::vector<Entity*>& entities, const sf::Vector2f& mousePosView);
	void updateSelectedEntityInput(const float& dt, const sf::Vector2f& mousePosView);

	void update();
	void render(sf::RenderTarget* target);
};