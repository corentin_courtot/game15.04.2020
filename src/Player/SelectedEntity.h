#pragma once
#include "Entity.h"

class SelectedEntity
{
private:
	sf::Sprite selectedEntitySprite;
	sf::Texture selectedEntityTexture;

	bool isEmpty;

public:
	std::vector<Entity*> selectedEntity;

	SelectedEntity();
	~SelectedEntity();

	bool isSet();

	void reset();

	void updateSelectedEntityInput(const float& dt, sf::Vector2f mousePosView);

	void addEntity(Entity* entity);
	void render(sf::RenderTarget* target);
};
