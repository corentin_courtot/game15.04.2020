#pragma once

class StatisticComponent
{
private:
	sf::Sprite& referenceSprite;

	sf::Sprite healthBar;
	sf::Texture texture;

	sf::Vector2f textureSize;
	sf::Vector2f healthOffset;
	sf::Vector2i textureOrigin;
	sf::IntRect healthTextureRect;

	unsigned short maxHealth;
	unsigned short health;
	float naturalRegen;

	float time;

public:
	StatisticComponent(sf::Sprite& referenceSprite);
	StatisticComponent(sf::Sprite& referenceSprite, unsigned short max_health, float naturalRegen);
	~StatisticComponent();

	void initStatisticComponent();
	void initTexture(std::string fileName);

	void addHealth(const short addHealth);

	void updateHealth();
	/*******************************************************
	 * 					Accessors
	 * *****************************************************/
	unsigned short getHealth() const;
	void setHealth(const unsigned short health);

	/******************************************************
	 * 					Rendering
	 ****************************************************/
	void update(const float& dt);
	void render(sf::RenderTarget& target) const;
};
