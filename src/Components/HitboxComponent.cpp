
#include "HitboxComponent.h"

HitboxComponent::HitboxComponent(sf::Sprite& sprite,
	float offset_x, float offset_y,
	float width, float height, float angle) :
	sprite(sprite),
	offsetX(offset_x),
	offsetY(offset_y),
	angle(angle)
{
	this->hitbox.setPosition(this->sprite.getPosition().x + offsetX,
		this->sprite.getPosition().y + offsetY);
	this->hitbox.setSize(sf::Vector2f(width, height));
	this->hitbox.setOrigin(sf::Vector2f(width * 0.5f, height * 0.5f));
	this->hitbox.setRotation(this->angle);

	this->hitbox.setFillColor(sf::Color::Transparent);
	this->hitbox.setOutlineThickness(1.f);
	this->hitbox.setOutlineColor(sf::Color::Blue);
}

HitboxComponent::~HitboxComponent()
{
}

bool HitboxComponent::checkIntersect(const HitboxComponent* otherHitbox)
{
	if (otherHitbox)
		return this->hitbox.getGlobalBounds().intersects(otherHitbox->hitbox.getGlobalBounds());
	return false;
}

void HitboxComponent::update()
{
	this->hitbox.setPosition(this->sprite.getPosition().x + this->offsetX,
		this->sprite.getPosition().y + this->offsetY);
}

void HitboxComponent::render(sf::RenderTarget& target)
{
	target.draw(this->hitbox);
}
