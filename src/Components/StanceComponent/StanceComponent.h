#pragma once

class StanceComponent
{
private:
protected:
	class Stance
	{
	public:
		//Variables
		sf::Sprite& sprite;

		sf::Texture* textureSheet;

		sf::IntRect rect;

		int width;
		int height;

		Stance(sf::Sprite& sprite, int start_frame_x, int start_frame_y, int width, int height, sf::Texture* texture_sheet) :
			sprite(sprite),
			textureSheet(texture_sheet),
			width(width),
			height(height)
		{
			this->rect = sf::IntRect(start_frame_x, start_frame_y, width, height);

			this->sprite.setTexture(*this->textureSheet, true);
			this->sprite.setTextureRect(this->rect);
		}

		//Functions
		const void play()
		{
			this->sprite.setTexture(*this->textureSheet, true);
			this->sprite.setTextureRect(this->rect);
		}
	};

	sf::Sprite& sprite;

	sf::Texture textureSheet;

	Stance* lastStance;
	std::map<std::string, Stance*> stances;

public:
	StanceComponent(sf::Sprite& sprite);
	virtual ~StanceComponent();

	//Functions
	void setupStance(
		const std::string& fileName,
		const std::vector<std::string>& keys,
		const sf::Vector2f& textureSize,
		const float stanceWidth);

	void addStance(const std::string key,
		int start_frame_x, int start_frame_y, int width, int height,
		sf::Texture* textureSheet);

	const virtual void play(const std::string key);
};
