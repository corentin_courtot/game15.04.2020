
#include "StanceComponent.h"

StanceComponent::StanceComponent(sf::Sprite& sprite) :
	sprite(sprite),
	lastStance(NULL)
{
	this->textureSheet = sf::Texture();
}

StanceComponent::~StanceComponent()
{
	for (auto& i : this->stances)
	{
		delete i.second;
	}
}

void StanceComponent::addStance(const std::string key, int start_frame_x, int start_frame_y, int width, int height, sf::Texture* textureSheet)
{
	this->stances[key] = new Stance(this->sprite, start_frame_x, start_frame_y, width, height, textureSheet);
}

void StanceComponent::setupStance(
	const std::string& fileName,
	const std::vector<std::string>& keys,
	const sf::Vector2f& textureSize,
	const float stanceWidth)
{
	// petite verification sur les entrees pour verifier si elles sont conformes
	if (keys.size() != textureSize.x / stanceWidth)
	{
		std::cout << "erreur de taille de texture dans setup stance" << std::endl;
		return;
	}

	// chargement de la texture
	this->textureSheet.loadFromFile(fileName);
	this->textureSheet.setSmooth(true);

	// creation des stances
	for (size_t i = 0; i < keys.size(); ++i)
	{
		addStance(keys[i], i * stanceWidth, 0, stanceWidth, textureSize.y, &this->textureSheet);
	}
}

const void StanceComponent::play(const std::string key)
{
	this->lastStance = this->stances[key];

	this->stances[key]->play();
}
