#pragma once

#include "BodyPart.h"
#include "EquipmentComponent.h"
#include "StanceComponent.h"

class CharacterStanceComponent : public StanceComponent
{
private:
	sf::Texture textureSheetNaked;
	sf::Texture textureSheetWithWeapon;

public:
	CharacterStanceComponent(sf::Sprite& sprite);
	virtual ~CharacterStanceComponent();

	void setupCharacterStance(BodyPart* head, BodyPart* body, BodyPart* hair);
	void updateCharacterTexture(const std::vector<Equipment*>& equipment, const Weapon* Weapon);
};
