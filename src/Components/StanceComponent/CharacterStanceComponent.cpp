#include "CharacterStanceComponent.h"

CharacterStanceComponent::CharacterStanceComponent(sf::Sprite& sprite) :
	StanceComponent(sprite)
{
	this->textureSheetNaked = sf::Texture();
	this->textureSheetWithWeapon = sf::Texture();
}

CharacterStanceComponent::~CharacterStanceComponent()
{
}

void CharacterStanceComponent::setupCharacterStance(BodyPart* head, BodyPart* body, BodyPart* hair)
// creation de l'apparence du personnage
{
	{
		sf::RenderTexture renderTexture = sf::RenderTexture();

		renderTexture.create(120, 60);
		renderTexture.clear(sf::Color::Transparent);

		sf::Sprite Body;
		Body.setTexture(body->texture);
		Body.setRotation(180);
		Body.move(sf::Vector2f(120.f, 60.f));
		renderTexture.draw(Body);

		sf::Sprite Head;
		Head.setTexture(head->texture);
		Head.setRotation(180);
		Head.move(sf::Vector2f(120.f, 60.f));
		renderTexture.draw(Head);

		if (hair)
		{
			sf::Sprite Hair;
			Hair.setTexture(hair->texture);
			Hair.setRotation(180);
			Hair.move(sf::Vector2f(120.f, 60.f));
			renderTexture.draw(Hair);
		}

		this->textureSheetNaked = renderTexture.getTexture();
		this->textureSheet = renderTexture.getTexture();
		this->textureSheetWithWeapon = renderTexture.getTexture();
	}

	addStance("frontWeapon", 90, 0, 30, 60, &this->textureSheetWithWeapon);
	addStance("facingLeftWeapon", 60, 0, 30, 60, &this->textureSheetWithWeapon);
	addStance("facingRightWeapon", 0, 0, 30, 60, &this->textureSheetWithWeapon);
	addStance("backWeapon", 30, 0, 30, 60, &this->textureSheetWithWeapon);

	addStance("front", 90, 0, 30, 60, &this->textureSheet);
	addStance("facingLeft", 60, 0, 30, 60, &this->textureSheet);
	addStance("facingRight", 0, 0, 30, 60, &this->textureSheet);
	addStance("back", 30, 0, 30, 60, &this->textureSheet);
}

void CharacterStanceComponent::updateCharacterTexture(const std::vector<Equipment*>& equipment, const Weapon* weapon)
{
	sf::RenderTexture renderTexture;

	renderTexture.create(120, 60);
	renderTexture.clear(sf::Color::Transparent);

	sf::Sprite naked;
	naked.setTexture(this->textureSheetNaked);
	naked.setScale(1.f, -1.f);
	naked.setPosition(0.f, 60.f);
	renderTexture.draw(naked);

	for (size_t i = 0; i < equipment.size(); i++)
	{
		if ((equipment)[i])
		{
			if ((equipment)[i]->getTexture())
			{
				sf::Sprite sprite;
				// recuperation de la texture de l'equipement
				sprite.setTexture(*(equipment)[i]->getTexture());

				// setup de la texture de l'equipement dans la rendertexture pour qu'il apparaisse comme voulu
				sprite.setRotation(180);
				sprite.move(sf::Vector2f(120.f, 60.f));

				renderTexture.draw(sprite);
			}
		}
	}
	this->textureSheet = renderTexture.getTexture();

	if (weapon && weapon->getTexture())
	{
		sf::Sprite sprite;
		// recuperation de la texture de l'equipement
		sprite.setTexture(*weapon->getTexture());

		// setup de la texture de l'equipement dans la rendertexture pour qu'il apparaisse comme voulu
		sprite.setRotation(180);
		sprite.move(sf::Vector2f(120.f, 60.f));

		renderTexture.draw(sprite);
	}

	this->textureSheetWithWeapon = renderTexture.getTexture();
}