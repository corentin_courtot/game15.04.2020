
#include "StatisticComponent.h"
StatisticComponent::StatisticComponent(sf::Sprite& referenceSprite) :
	referenceSprite(referenceSprite),
	maxHealth(100),
	naturalRegen(2.f)
{
	this->initStatisticComponent();
}
StatisticComponent::StatisticComponent(sf::Sprite& referenceSprite, unsigned short health, float naturalRegen) :
	referenceSprite(referenceSprite),
	maxHealth(health),
	naturalRegen(naturalRegen)
{
	this->initStatisticComponent();
}

StatisticComponent::~StatisticComponent()
{
}

void StatisticComponent::initStatisticComponent()
{
	this->time = 0.f;
	this->health = this->maxHealth;
	this->initTexture("HealthBar");
}

void StatisticComponent::initTexture(std::string fileName)
{
	this->texture.loadFromFile("Ressources/Images/Sprites/Effects/" + fileName + ".png");
	this->texture.setSmooth(true);
	this->textureSize = sf::Vector2f(60.f, 3.f);
	this->healthBar.setTexture(texture, true);
	this->textureOrigin = sf::Vector2i(30, 14);

	this->healthBar.setOrigin(this->textureSize.x / 4.f, this->textureSize.y / 2.f);
	this->healthOffset.x = 0;
	this->healthOffset.y = -50;

	this->healthTextureRect.left = this->textureOrigin.x;
	this->healthTextureRect.top = this->textureOrigin.y;
	this->healthTextureRect.width = this->textureSize.x / 2.f;
	this->healthTextureRect.height = this->textureSize.y;

	this->healthBar.setTextureRect(this->healthTextureRect);

	this->healthBar.setScale(1.5f, 1.5f);
}

void StatisticComponent::addHealth(const short addHealth)
{
	if (this->health + addHealth < 0)
		this->health = 0;

	else if (this->health + addHealth > this->maxHealth)
		this->health = this->maxHealth;

	else
		this->health += addHealth;
}

void StatisticComponent::updateHealth()
{
	if (time >= 1.f)
	{
		this->addHealth(this->naturalRegen);
	}
	this->healthBar.setPosition(this->referenceSprite.getPosition() + healthOffset);
	this->healthTextureRect.left = this->textureOrigin.x + (this->textureSize.x / 2.f - this->textureSize.x / 2.f * this->health / (float)this->maxHealth);
	this->healthBar.setTextureRect(this->healthTextureRect);
}
/*********************************************************
 * 				RENDERING
 *********************************************************/
void StatisticComponent::update(const float& dt)
{
	time += dt;
	this->updateHealth();
	if (time >= 1.f)
		time = 0.f;
}

void StatisticComponent::render(sf::RenderTarget& target) const
{
	target.draw(this->healthBar);
}

/*******************************************************
 * 					Accessors
 * *****************************************************/
unsigned short StatisticComponent::getHealth() const
{
	return this->health;
}
void StatisticComponent::setHealth(const unsigned short health)
{
	if (health < 0)
		this->health = 0;

	else if (health > this->maxHealth)
		this->health = this->maxHealth;

	else
		this->health = health;
}