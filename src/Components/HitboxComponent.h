#pragma once

class HitboxComponent
{
private:
	sf::Sprite& sprite;
	sf::RectangleShape hitbox;

	float offsetX;
	float offsetY;

	float angle;

public:
	HitboxComponent(sf::Sprite& sprite,
		float offset_x, float offset_y,
		float width, float height, float angle = 0.f);
	virtual ~HitboxComponent();

	//Functions
	bool checkIntersect(const HitboxComponent* otherHitbox);

	void update();
	void render(sf::RenderTarget& target);
};
