
#include "MovementComponent.h"

MovementComponent::MovementComponent(sf::Sprite& sprite, const float maxVelocity) :
	sprite(sprite),
	maxVelocity(maxVelocity),
	velocity(sf::Vector2f(0.f, 0.f)),
	target(NULL)
{
}

MovementComponent::~MovementComponent()
{
	if (target)
	{
		delete this->target;
		this->target = NULL;
	}
}

//Accessors
const float& MovementComponent::getMaxVelocity() const
{
	return this->maxVelocity;
}

sf::Vector2f MovementComponent::getVelocity() const
{
	return this->velocity;
}

sf::Vector2f* MovementComponent::getTarget() const
{
	return this->target;
}

void MovementComponent::setMovementTarget(sf::Vector2f target)
{
	if (this->target == NULL)
	{
		this->target = new sf::Vector2f;
	}

	*this->target = target;

	this->velocity = (*this->target - this->sprite.getPosition())
		* this->maxVelocity
		/ dist(*this->target - this->sprite.getPosition());
}

void MovementComponent::updateTarget()
{
	if (target && dist(*this->target - this->sprite.getPosition()) < 2)
	{
		delete this->target;
		this->target = NULL;
		this->velocity = sf::Vector2f(0.f, 0.f);
	}
}

const movement_states MovementComponent::getState() const
{
	if (this->velocity.x == 0.f && this->velocity.y == 0.f)
		return IDLE;
	else if (this->velocity.y < 0.f && -this->velocity.y >= abs(this->velocity.x) * 2.5f)
		return WALKING_UP;
	else if (this->velocity.y > 0.f && this->velocity.y >= abs(this->velocity.x) * 2.5f)
		return WALKING_DOWN;
	else if (this->velocity.x > 0.f)
		return WALKING_RIGHT;
	else if (this->velocity.x < 0.f)
		return WALKING_LEFT;

	return DEFAULT;
}

//Functions
/*void MovementComponent::move(const float dir_x, const float dir_y, const float& dt)
{
	//Acceleration
	this->velocity.x += this->acceleration * dir_x;
	this->velocity.y += this->acceleration * dir_y;
}*/

void MovementComponent::update(const float& dt)
{
	this->updateTarget();

	if (target)
	{
		this->velocity = (*this->target - this->sprite.getPosition())
			* this->maxVelocity
			/ dist(*this->target - this->sprite.getPosition());

		this->sprite.move(this->velocity * dt);
	}
}
