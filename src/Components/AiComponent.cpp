#include "AiComponent.h"

/*			Random number generator			*/
void AiComponent::srand(int seed)
{
	this->state = seed;
}

int AiComponent::rand()
{
	int const a = 1103515245;
	int const c = 12345;
	this->state = a * this->state + c;
	return (this->state >> 16) & 0x7FFF;
}

void AiComponent::setMovementTarget(sf::Vector2f target)
{
	this->movementComponent->setMovementTarget(IndTileToVector2f(vector2fToIndTile(target)));
}

void AiComponent::randomMovement(sf::Vector2f pos, GameMap* gameMap)
{
	std::cout << "(AiComponent) randomMovement not define for this Ai" << std::endl;
}

/*			Constructor					*/
AiComponent::AiComponent(MovementComponent* movementComponent, const int AiSeed) :
	state(0),
	movementState(0),
	time(0.f),
	movementComponent(movementComponent)

{
	this->srand(AiSeed);
}

AiComponent::~AiComponent()
{
}

/*			Update						*/
void AiComponent::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	this->time += dt;
	if (this->time >= 1.f)
	{
		this->time = 0;
		this->movementState = true;
	}
}

/*			Accessors					*/
int AiComponent::getSeed() const
{
	return this->state;
}
void AiComponent::setSeed(const int seed)
{
	this->state = seed;
}