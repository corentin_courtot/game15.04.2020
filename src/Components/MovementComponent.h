#pragma once

#include "GeneralFunctions.h"

enum movement_states
{
	DEFAULT = 0,
	IDLE,
	WALKING_UP,
	WALKING_DOWN,
	WALKING_LEFT,
	WALKING_RIGHT
};

class MovementComponent
{
private:
	sf::Sprite& sprite;

	float maxVelocity;

	sf::Vector2f velocity;
	sf::Vector2f* target;

	// Initializer functions

public:
	MovementComponent(sf::Sprite& sprite, const float maxVelocity);
	virtual ~MovementComponent();

	//Accessors
	const float& getMaxVelocity() const;
	sf::Vector2f getVelocity() const;
	sf::Vector2f* getTarget() const;

	void setMovementTarget(sf::Vector2f target);
	void updateTarget();

	//Functions
	const movement_states getState() const;

	void update(const float& dt);
};
