#pragma once

#include "Entity.h"
#include "GameMap.h"
#include "MovementComponent.h"

class AiComponent
{
protected:
	int state;
	static const int randMax = 32767;
	bool movementState;
	float time;

	MovementComponent* movementComponent;
	/*			Random number generator			*/
	void srand(int seed);
	int rand();

	void setMovementTarget(sf::Vector2f target);

public:
	virtual void randomMovement(sf::Vector2f pos,GameMap* gameMap);

	/*			Constructor					*/
	AiComponent(MovementComponent* movementComponent, const int AiSeed);
	~AiComponent();

	/*			Update						*/
	virtual void update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap);

	/*			Accessors					*/
	int getSeed() const;
	void setSeed(const int seed);
};
