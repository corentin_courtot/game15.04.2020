#pragma once

float dist(float a, float b);

template <typename T>
T dist(sf::Vector2<T> v)
{
	return sqrt(v.x * v.x + v.y * v.y);
}

template <typename T>
T distSquared(sf::Vector2<T> v)
{
	return v.x * v.x + v.y * v.y;
}

template <typename T>
T squareDist(sf::Vector2<T> v)
{
	return abs(v.x) + abs(v.y);
}

template <typename T>
T positif(T t)
{
	if (t > 0)
		return t;
	return 0;
}

sf::Vector2i vector2fToIndTile(const sf::Vector2f& v);
sf::Vector2f IndTileToVector2f(const sf::Vector2i& v);
int pixelDistToTileDist(int d);

namespace myRand
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
static int state = 0;
#pragma GCC diagnostic pop
static const int randMax = 32767;

void srand(int seed);

int rand();
}