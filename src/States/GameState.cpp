
#include "GameState.h"

//Initializer Functions
void GameState::initView()
{
	if (this->entities.size() > 0)
		this->gameView.setCenter(this->entities[0]->getCoord() + this->entities[0]->getTextureSize() / 2.f);
	this->gameView.setSize(sf::Vector2f(1920, 1080));

	this->zoom = 1.f;
}

void GameState::initFonts()
{
	if (!this->font.loadFromFile("Fonts/Dosis-Light.ttf"))
	{
		throw("ERROR::MAINMENUSTATE::COULD NOT LOAD FONT");
	}
}

void GameState::initKeybinds()
{
	std::ifstream ifs("Config/gamestate_keybinds.ini");

	if (ifs.is_open())
	{
		std::string key = "";
		std::string key2 = "";

		while (ifs >> key >> key2)
		{
			this->keybinds[key] = this->supportedKeys->at(key2);
		}
	}

	ifs.close();
}

void GameState::initTextures()
{
}

void GameState::initPauseMenu()
{
	this->pmenu = NULL;
}

void GameState::initCharacters()
{
	this->entities.push_back(new Character(sf::Vector2f(2050.f, 1500.f), head1, bodyF1, hairF1, FEMALE));
	this->entities.push_back(new Rabbit(sf::Vector2f(2050.f, 1400.f), freeId(this->entities), 1));
}

void GameState::initEntities()
{
	this->entities.resize(0);
	this->mapEntities.resize(0);
	//this->initCharacters();
}

void GameState::resetPauseMenu()
{
	if (pmenu)
		delete this->pmenu;

	this->pmenu = new PauseMenu(*this->window, this->gameView, this->font);
}

//Constructor/Destructors
GameState::GameState(
	sf::RenderWindow* window,
	std::map<std::string, int>* supportedKeys,
	std::stack<State*>* states,
	unsigned short seed,
	unsigned short id) :
	State(window, supportedKeys, states),
	player(id)
{

	this->initFonts();
	this->initKeybinds();
	this->initTextures();

	this->initEntities();

	this->initView();
	this->initPauseMenu();

	this->gameMap = new GameMap(tileSize, mapSize.x, mapSize.y, seed, this->entities, this->mapEntities);
	this->visibilityMap = new VisibilityMap(mapSize);
}

GameState::~GameState()
{
	delete this->pmenu;
	for (size_t i = 0; i < this->entities.size(); i++)
	{
		delete this->entities[i];
		this->entities[i] = 0;
	}
	this->entities.resize(0);
	for (size_t i = 0; i < this->mapEntities.size(); i++)
	{
		delete this->mapEntities[i];
		this->mapEntities[i] = 0;
	}
	this->mapEntities.resize(0);
}

//* Network
void GameState::addEntity(DataTransmitted& data)
{
	if (data.objectType == EntityType::player)
	{
		this->entities.push_back(new Character(data, data.belongId == this->player.getId()));
	}
	else if (data.objectType == arrow)
	{
		this->entities.push_back(new Projectile(data));
	}
	else if (data.objectType == hitEffect)
	{
		this->entities.push_back(new Effect(data));
	}
	else if (data.objectType == rabbit)
	{
		this->entities.push_back(new Rabbit(data.pos, data.id, data.belongId));
	}
}

void GameState::readServerData(std::queue<DataTransmitted>& fromServer, std::mutex& fromServerLock)
{
	fromServerLock.lock();

	if (fromServer.empty())
	{
		fromServerLock.unlock();
		return;
	}

	size_t i = 0;
	std::vector<DataTransmitted> toAdd;

	// std::cout << std::endl;
	// std::cout << "(GameState) data received : fromServer size=" << fromServer.size() << std::endl;

	// comparing incoming data from server and loal data
	while (fromServer.size() > 0 && i < this->entities.size())
	{
		// std::cout << "objectType:" << fromServer.front().objectType
		// 		  << " id:" << fromServer.front().id
		// 		  << " scale:" << fromServer.front().scale.x
		// 		  << " health:" << fromServer.front().health
		// 		  << " pos:" << fromServer.front().pos.x << "," << fromServer.front().pos.y
		// 		  << " target:" << fromServer.front().target.x << "," << fromServer.front().target.y
		// 		  << " state:" << fromServer.front().state << std::endl;

		if (fromServer.front().id == this->entities[i]->getId())
		// if the entity exists, just update it
		{
			this->entities[i]->updateAccordingToServerData(fromServer.front());
			++i;
			fromServer.pop();
		}
		else if (fromServer.front().id > this->entities[i]->getId())
		// if an entity had disappeared
		{
			delete this->entities[i];
			this->entities[i] = NULL;
			++i;
		}
		else if (fromServer.front().id < this->entities[i]->getId())
		// if an entity has been added
		{
			toAdd.push_back(fromServer.front());
			fromServer.pop();
		}
	}

	// if there are some entities left in fromServer
	while (fromServer.size() > 0)
	{
		// std::cout << "adding new entity : "
		// 		  << "objectType:" << fromServer.front().objectType
		// 		  << " id:" << fromServer.front().id
		// 		  << " scale:" << fromServer.front().scale
		// 		  << " pos:" << fromServer.front().pos.x << "," << fromServer.front().pos.y
		// 		  << " target:" << fromServer.front().target.x << "," << fromServer.front().target.y
		// 		  << " state:" << fromServer.front().state << std::endl;

		toAdd.push_back(fromServer.front());
		fromServer.pop();
	}

	// or if more entities are locally defined
	while (i < this->entities.size())
	{
		delete this->entities[i];
		this->entities[i] = NULL;
		++i;
	}

	// finally, add toAdd to the local entities vect
	for (size_t i = 0; i < toAdd.size(); i++)
	{
		this->addEntity(toAdd[i]);
	}

	fromServerLock.unlock();
}

void GameState::uploadLocalEntitiesToServer(std::queue<DataTransmitted>& toServer)
{
	for (size_t i = 0; i < this->entities.size(); i++)
	{
		if (this->entities[i] && this->entities[i]->getBelongId() == this->player.getId())
		{
			// std::cout << "(GameState) entity pushed toServer : "
			// 		  << "id : " << this->entities[i]->getId()
			// 		  << " belongId : " << this->entities[i]->getBelongId()
			// 		  << std::endl;
			DataTransmitted data = DataTransmitted();

			this->entities[i]->toDataTransmitted(data);

			toServer.push(data);
		}
	}
}

//* Update functions
void GameState::updateInput(const float& dt)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("CLOSE"))) && this->getKeytime())
	{
		if (!this->paused)
		{
			resetPauseMenu();
			this->pauseState();
		}
		else
			this->unPauseState();
	}
}

void GameState::updateViewInput(const float& dt, const float wheelTicks)
{
	//Update view position
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_VIEW_UP")))
		|| this->mousePosScreen.y < this->window->getSize().y * 0.05f)
		this->gameView.move(sf::Vector2f(0.f, -1000.f * dt * this->zoom));

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_VIEW_DOWN")))
		|| this->mousePosScreen.y > this->window->getSize().y * 0.95f)
		this->gameView.move(sf::Vector2f(0.f, 1000.f * dt * this->zoom));

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_VIEW_LEFT")))
		|| this->mousePosScreen.x < this->window->getSize().x * 0.05f)
		this->gameView.move(sf::Vector2f(-1000.f * dt * this->zoom, 0.f));

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(this->keybinds.at("MOVE_VIEW_RIGHT")))
		|| this->mousePosScreen.x > this->window->getSize().x * 0.95f)
		this->gameView.move(sf::Vector2f(1000.f * dt * this->zoom, 0.f));

	//Update zoom
	if (wheelTicks != 0)
	{
		if ((wheelTicks > 0 && gameView.getSize().x > 500.f) || (wheelTicks < 0 && gameView.getSize().x < maxViewSize.x))
		{
			this->gameView.zoom(1.f - wheelTicks / 10.f);
			if (this->gameView.getSize().x > maxViewSize.x)
			{
				this->gameView.setSize(maxViewSize);
			}
			if (this->gameView.getSize().x < 500.f)
			{
				this->gameView.setSize(sf::Vector2f(500.f, 281.25f));
			}
		}
	}
}

void GameState::updatePauseMenuTextButtons(std::queue<DataTransmitted>& toServer)
{
	if (this->pmenu->isTextButtonPressed("QUIT"))
	{
		while (toServer.size() > 0)
		{
			toServer.pop();
		}
		toServer.push(DataTransmitted(0, 2)); // inform the server that the client has quit the game

		this->endState();
	}
}

void GameState::updateView(const float& dt, const float wheelTicks)
{
	this->updateViewInput(dt, wheelTicks);
}

void GameState::update(
	const float& dt,
	std::queue<DataTransmitted>& toServer,
	std::queue<DataTransmitted>& fromServer,
	std::mutex& fromServerLock,
	const appMode& mode,
	const bool& isConnected,
	const float wheelTicks)
{
	// debug
	//std::cout << "(GameState) entities display : " << std::endl;
	//displayEntities(this->entities);

	this->updateMousePositions();
	this->updateKeytime(dt);

	this->updateInput(dt);

	if (!this->paused) // unpaused update
	{
		this->player.updateSelectedEntity(this->entities, this->mousePosView);
		this->player.updateSelectedEntityInput(dt, this->mousePosView);
		this->updateView(dt, wheelTicks);

		deleteHolesAndSort(this->entities); // tri et cleanup de entities

		// Network
		if (isConnected)
		{
			this->uploadLocalEntitiesToServer(toServer);
			this->readServerData(fromServer, fromServerLock);
		}

		for (size_t i = 0; i < this->entities.size(); i++)
		{
			if (this->entities[i])
			{
				if (!this->entities[i]->update(dt, this->entities, this->mapEntities, this->gameMap, this->visibilityMap))
				{
					delete this->entities[i];
					this->entities[i] = NULL;
				}
			}
		}
		for (size_t i = 0; i < this->mapEntities.size(); i++)
		{
			if (this->mapEntities[i])
			{
				if (!this->mapEntities[i]->update(dt, this->entities, this->mapEntities, this->gameMap, this->visibilityMap))
				{
					delete this->mapEntities[i];
					this->mapEntities[i] = NULL;
				}
			}
		}
	}
	else // paused update
	{
		this->pmenu->update(this->mousePosView);
		this->updatePauseMenuTextButtons(toServer);
	}
}

void GameState::render(const float& dt, const bool& isConnected)
{
	this->window->setView(this->gameView);

	this->gameMap->render(*this->window, vector2fToIndTile(this->gameView.getCenter()));
	this->visibilityMap->render(*this->window, vector2fToIndTile(this->gameView.getCenter()));

	this->player.render(this->window);

	// 2 priorites de render
	for (size_t i = 0; i < this->entities.size(); i++)
	{
		if (this->entities[i] && !this->entities[i]->getRenderingPriority())
		{
			this->entities[i]->render(*this->window);
			this->entities[i]->isInViewRange = false;
		}
	}
	for (size_t i = 0; i < this->entities.size(); i++)
	{
		if (this->entities[i] && this->entities[i]->getRenderingPriority())
		{
			this->entities[i]->render(*this->window);
			this->entities[i]->isInViewRange = false;
		}
	}

	for (size_t i = 0; i < this->mapEntities.size(); i++)
	{
		if (this->mapEntities[i] && !this->mapEntities[i]->getRenderingPriority())
		{
			this->mapEntities[i]->render(*this->window);
			this->mapEntities[i]->isInViewRange = false;
		}
	}
	for (size_t i = 0; i < this->mapEntities.size(); i++)
	{
		if (this->mapEntities[i] && this->mapEntities[i]->getRenderingPriority())
		{
			this->mapEntities[i]->render(*this->window);
			this->mapEntities[i]->isInViewRange = false;
		}
	}

	if (this->paused) // Pause menu render
	{
		this->pmenu->render(*this->window);
	}

	// Remove later! : display DPS
	if (dt != 0.f)
	{
		sf::Text FPS;
		FPS.setString("FPS : " + std::to_string((int)(1 / dt)));
		sf::Vector2f offset(-900.f, -450.f);
		FPS.setPosition(this->gameView.getCenter() + offset);
		FPS.setFont(this->font);
		FPS.setCharacterSize(18);
		this->window->draw(FPS);
	}

	// sf::Text mouseText;
	// mouseText.setPosition(this->mousePosView.x, this->mousePosView.y - 30);
	// mouseText.setFont(this->font);
	// mouseText.setCharacterSize(40);
	// std::stringstream ss;
	// sf::Vector2i ind = vector2fToIndTile(this->mousePosView);

	// if (ind.x < 0)
	// 	ind.x = 0;
	// if (ind.x >= this->gameMap->getMapSize().x)
	// 	ind.x = this->gameMap->getMapSize().x - 1;
	// if (ind.y < 0)
	// 	ind.y = 0;
	// if (ind.y >= this->gameMap->getMapSize().y)
	// 	ind.y = this->gameMap->getMapSize().y - 1;

	// ss << (int)this->gameMap->tiles[ind.x][ind.y].getHeight() << "\n"
	//    << (float)this->gameMap->HeightGrad->getGradient(ind.x, ind.y, this->gameMap->getMapSize()); // << " " << this->mousePosView.y;
	// mouseText.setString(ss.str());
	// this->window->draw(mouseText);
}
