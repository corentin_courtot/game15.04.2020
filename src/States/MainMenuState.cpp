
#include "MainMenuState.h"

//Initializer functions
void MainMenuState::initVariables()
{
}

void MainMenuState::initBackground()
{
	this->background.setSize(
		sf::Vector2f(
			static_cast<float>(this->window->getSize().x),
			static_cast<float>(this->window->getSize().y)));

	if (!this->backgroundTexture.loadFromFile("Ressources/Images/Background/background.png"))
	{
		throw "ERROR::MAIN_MENU_STATE::FAILED_TO_LOAD_BACKGROUND_TEXTURE";
	}
	this->background.setTexture(&backgroundTexture);
}

void MainMenuState::initFonts()
{
	if (!this->font.loadFromFile("Fonts/Dosis-Light.ttf"))
	{
		throw("ERROR::MAINMENUSTATE::COULD NOT LOAD FONT");
	}
}

void MainMenuState::initKeybinds()
{
	std::ifstream ifs("Config/mainmenustate_keybinds.ini");

	if (ifs.is_open())
	{
		std::string key = "";
		std::string key2 = "";

		while (ifs >> key >> key2)
		{
			this->keybinds[key] = this->supportedKeys->at(key2);
		}
	}

	ifs.close();
}

void MainMenuState::initSound()
{
	if (!backgroundMusic.openFromFile("Ressources/Audio/airtone_reCreation.ogg"))
		throw("ERROR::MAINMENUSTATE::COULD NOT LOAD BACKGROUNDMUSIC");

	if (!buttonHoverSoundBuffer.loadFromFile("Ressources/Audio/button_mainMenu.ogg"))
		throw("ERROR::MAINMENUSTATE::COULD NOT LOAD BUTTONHOVERSOUND");

	if (!buttonActiveSoundBuffer.loadFromFile("Ressources/Audio/button_mainMenu.ogg"))
		throw("ERROR::MAINMENUSTATE::COULD NOT LOAD BUTTONACTIVESOUND");
}

void MainMenuState::initTextButtons()
{
	this->buttons["GAME_STATE"] = new gui::TextButton(
		880.f, 400.f, 150.f, 50.f, &this->font, "New Game", 50, sf::Color(70, 70, 70, 200), sf::Color(250, 250, 250, 250), sf::Color(20, 20, 20, 50), sf::Color(70, 70, 70, 0), sf::Color(150, 150, 150, 0), sf::Color(20, 20, 20, 0), &this->buttonHoverSoundBuffer, &this->buttonActiveSoundBuffer);

	this->buttons["SETTINGS_STATE"] = new gui::TextButton(
		880, 470.f, 150.f, 50.f, &this->font, "Settings", 50, sf::Color(70, 70, 70, 200), sf::Color(250, 250, 250, 250), sf::Color(20, 20, 20, 50), sf::Color(70, 70, 70, 0), sf::Color(150, 150, 150, 0), sf::Color(20, 20, 20, 0), &this->buttonHoverSoundBuffer, &this->buttonActiveSoundBuffer);

	this->buttons["EDITOR_STATE"] = new gui::TextButton(
		880, 540.f, 150.f, 50.f, &this->font, "Editor", 50, sf::Color(70, 70, 70, 200), sf::Color(250, 250, 250, 250), sf::Color(20, 20, 20, 50), sf::Color(70, 70, 70, 0), sf::Color(150, 150, 150, 0), sf::Color(20, 20, 20, 0), &this->buttonHoverSoundBuffer, &this->buttonActiveSoundBuffer);

	this->buttons["EXIT_STATE"] = new gui::TextButton(
		880, 630.f, 150.f, 50.f, &this->font, "Quit", 50, sf::Color(70, 70, 70, 200), sf::Color(250, 250, 250, 250), sf::Color(20, 20, 20, 50), sf::Color(70, 70, 70, 0), sf::Color(150, 150, 150, 0), sf::Color(20, 20, 20, 0), &this->buttonHoverSoundBuffer, &this->buttonActiveSoundBuffer);
}

void MainMenuState::initGame(
	std::queue<DataTransmitted>& toServer,
	std::queue<DataTransmitted>& fromServer,
	std::mutex& fromServerLock)
{
	if (fromServer.size() == 0)
	{
		std::cout << "(MainMenuState) Launching game with no connection" << std::endl;

		myRand::srand(time(NULL));

		this->states->push(new GameState(this->window,
			this->supportedKeys,
			this->states,
			myRand::rand(),
			1));
	}
	else
	{
		std::cout << "(MainMenuState) Init game with seed " << fromServer.front().objectType
				  << " and player id " << fromServer.front().id << std::endl;

		this->states->push(new GameState(this->window,
			this->supportedKeys,
			this->states,
			fromServer.front().objectType,
			fromServer.front().id));

		while (fromServer.size() > 0)
			fromServer.pop();

		toServer.push(DataTransmitted(0, 1));
	}
}

MainMenuState::MainMenuState(sf::RenderWindow* window, GraphicsSettings& gfxSettings, std::map<std::string, int>* supportedKeys, std::stack<State*>* states) :
	State(window, supportedKeys, states),
	gfxSettings(gfxSettings)
{
	this->initVariables();
	this->initBackground();
	this->initFonts();
	this->initKeybinds();
	this->initSound();
	this->initTextButtons();
}

MainMenuState::~MainMenuState()
{
	this->backgroundMusic.stop();

	auto it = this->buttons.begin();
	for (it = this->buttons.begin(); it != this->buttons.end(); ++it)
	{
		delete it->second;
	}
}

void MainMenuState::updateInput(const float& dt)
{
}

void MainMenuState::updateTextButtons(
	std::queue<DataTransmitted>& toServer,
	std::queue<DataTransmitted>& fromServer,
	std::mutex& fromServerLock)
{
	/*Updates all the buttons in the state and handles their functionlaity.*/

	for (auto& it : this->buttons)
	{
		it.second->update(this->mousePosView);
	}

	//New game
	if (this->buttons["GAME_STATE"]->isPressed())
	{
		this->backgroundMusic.stop();
		this->initGame(toServer, fromServer, fromServerLock);
	}

	//Settings
	if (this->buttons["SETTINGS_STATE"]->isPressed())
	{
		this->states->push(new SettingsState(this->window, this->gfxSettings, this->supportedKeys, this->states)); //new GameState(this->window, &this->supportedKeys, this->states));
	}

	//Editor
	if (this->buttons["EDITOR_STATE"]->isPressed())
	{
		this->states->push(new EditorState(this->window, this->supportedKeys, this->states)); //new GameState(this->window, &this->supportedKeys, this->states));
	}

	//Quit the game
	if (this->buttons["EXIT_STATE"]->isPressed())
	{
		this->backgroundMusic.stop();
		this->endState();
	}
}

void MainMenuState::updateMusic()
{
	if (this->backgroundMusic.getStatus() == sf::SoundSource::Stopped)
	{
		this->backgroundMusic.setPlayingOffset(sf::seconds(5.2f));
		this->backgroundMusic.play();
	}
}

void MainMenuState::update(
	const float& dt,
	std::queue<DataTransmitted>& toServer,
	std::queue<DataTransmitted>& fromServer,
	std::mutex& fromServerLock,
	const appMode& mode,
	const bool& isConnected,
	const float wheelTicks)
{
	if ((mode == ALL || mode == CLIENT)
		&& !isConnected)
	{
		sf::sleep(sf::milliseconds(50));
	}

	this->updateMusic();

	this->updateMousePositions();
	this->updateInput(dt);

	this->updateTextButtons(toServer, fromServer, fromServerLock);
}

void MainMenuState::renderTextButtons(sf::RenderTarget& target)
{
	for (auto& it : this->buttons)
	{
		it.second->render(target);
	}
}

void MainMenuState::renderConnectionState(sf::RenderTarget& target, const bool& isConnected)
{
	sf::Text connectionText;
	connectionText.setFont(this->font);
	connectionText.setCharacterSize(15);
	sf::CircleShape connectionIndicator(5);
	connectionIndicator.setPosition(sf::Vector2f(1690.f, 55.f));

	if (isConnected)
	{
		connectionText.setString("connected");
		connectionText.setPosition(sf::Vector2f(1600.f, 50.f));
		connectionIndicator.setFillColor(sf::Color::Green);
	}
	else
	{
		connectionText.setString("looking for connection");
		connectionText.setPosition(sf::Vector2f(1550.f, 50.f));
		connectionIndicator.setFillColor(sf::Color::Red);
	}

	target.draw(connectionText);
	target.draw(connectionIndicator);
}

void MainMenuState::render(const float& dt, const bool& isConnected)
{
	this->window->draw(this->background);

	this->renderTextButtons(*this->window);

	this->renderConnectionState(*this->window, isConnected);

	////REMOVE LATER
	//sf::Text mouseText;
	//mouseText.setPosition(this->mousePosView.x, this->mousePosView.y - 15);
	//mouseText.setFont(this->font);
	//mouseText.setCharacterSize(12);
	//std::stringstream ss;
	//ss << this->mousePosView.x << " " << this->mousePosView.y;
	//mouseText.setString(ss.str());
	//target->draw(mouseText);
}
