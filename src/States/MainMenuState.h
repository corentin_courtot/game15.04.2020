#pragma once
#include "DataTransmitted.h"
#include "EditorState.h"
#include "GameState.h"
#include "GraphicsSettings.h"
#include "Gui.h"
#include "SettingsState.h"

class MainMenuState :
	public State
{
private:
	//Variables
	GraphicsSettings& gfxSettings;
	sf::Texture backgroundTexture;
	sf::RectangleShape background;
	sf::Font font;
	sf::Music backgroundMusic;
	sf::SoundBuffer buttonHoverSoundBuffer;
	sf::SoundBuffer buttonActiveSoundBuffer;

	std::map<std::string, gui::TextButton*> buttons;

	//Functions
	void initVariables();
	void initBackground();
	void initFonts();
	void initKeybinds();
	void initTextButtons();
	void initSound();

	void initGame(
		std::queue<DataTransmitted>& toServer,
		std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLock);

public:
	MainMenuState(sf::RenderWindow* window, GraphicsSettings& gfxSettings, std::map<std::string, int>* supportedKeys, std::stack<State*>* states);
	virtual ~MainMenuState();

	//Functions
	void updateInput(const float& dt);
	void updateTextButtons(
		std::queue<DataTransmitted>& toServer,
		std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLockk);
	void updateMusic();
	void update(
		const float& dt,
		std::queue<DataTransmitted>& toServer,
		std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLock,
		const appMode& mode,
		const bool& isConnected,
		const float wheelTicks = 0);

	void renderTextButtons(sf::RenderTarget& target);
	void renderConnectionState(sf::RenderTarget& target, const bool& isConnected);
	void render(const float& dt = 0.f, const bool& isConnected = true);
};
