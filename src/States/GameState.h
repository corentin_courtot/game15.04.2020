#pragma once

#include "Character.h"
#include "DataTransmitted.h"
#include "Effect.h"
#include "Entity.h"
#include "GameMap.h"
#include "PauseMenu.h"
#include "Projectile.h"
//#include "SelectedEntity.h"
#include "Fauna.h"
#include "Player.h"
#include "State.h"
#include "VisibilityMap.h"

class GameState :
	public State
{
private:
	// GUI
	sf::Font font;
	PauseMenu* pmenu;

	//SelectedEntity selection;
	Player player;

	// Elements on the map
	std::vector<Entity*> entities;
	std::vector<Entity*> mapEntities;

	// Map
	GameMap* gameMap;
	VisibilityMap* visibilityMap;

	// View
	sf::View gameView;
	float zoom;

	//Functions
	void initView();
	void initFonts();
	void initKeybinds();
	void initTextures();
	void initPauseMenu();

	void initCharacters();
	void initEntities();

	void resetPauseMenu();

public:
	GameState(sf::RenderWindow* window,
		std::map<std::string, int>* supportedKeys,
		std::stack<State*>* states,
		unsigned short seed = 0,
		unsigned short id = 0);
	virtual ~GameState();

	//* Network
	void addEntity(DataTransmitted& data);
	// reads fromServer & updates gamseState entities according to fromServer
	void readServerData(std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLock);
	// fills in toServer with local modifications of entities
	void uploadLocalEntitiesToServer(std::queue<DataTransmitted>& toServer);

	//* Update functions
	void updateInput(const float& dt);
	void updateViewInput(const float& dt, const float wheelTicks);
	void updatePauseMenuTextButtons(std::queue<DataTransmitted>& toServer);
	void updateView(const float& dt, const float wheelTicks);

	void update(
		const float& dt,
		std::queue<DataTransmitted>& toServer,
		std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLock,
		const appMode& mode,
		const bool& isConnected,
		const float wheelTicks);
	void render(const float& dt = 0.f, const bool& isConnected = true);
};