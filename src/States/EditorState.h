#pragma once

#include "DataTransmitted.h"
#include "GameState.h"
#include "Gui.h"

class EditorState :
	public State
{
private:
	//Variables
	sf::Font font;

	std::map<std::string, gui::TextButton*> buttons;

	//Functions
	void initVariables();
	void initFonts();
	void initKeybinds();
	void initTextButtons();

public:
	EditorState(sf::RenderWindow* window, std::map<std::string, int>* supportedKeys, std::stack<State*>* states);
	virtual ~EditorState();

	//Functions
	void updateInput(const float& dt);
	void updateTextButtons();
	void update(
		const float& dt,
		std::queue<DataTransmitted>& toServer,
		std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLock,
		const appMode& mode,
		const bool& isConnected,
		const float wheelTicks);
	void renderTextButtons(sf::RenderTarget& target);
	void render(const float& dt = 0.f, const bool& isConnected = true);
};