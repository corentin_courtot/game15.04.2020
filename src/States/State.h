#pragma once

#include "Character.h"
#include "DataTransmitted.h"
#include "GraphicsSettings.h"

class Character;
class GraphicsSettings;

enum appMode
{
	ALL = 0,
	CLIENT,
	SERVER_ONLY,
	GAME_ONLY
};

class State
{
private:
protected:
	std::stack<State*>* states;
	sf::RenderWindow* window;
	std::map<std::string, int>* supportedKeys;
	std::map<std::string, int> keybinds;
	bool quit;
	bool paused;
	float keytime;
	float keytimeMax;

	sf::Vector2i mousePosScreen;
	sf::Vector2i mousePosWindow;
	sf::Vector2f mousePosView;

	//Resources
	std::map<std::string, sf::Texture> textures;

	//Functions
	virtual void initKeybinds() = 0;

public:
	State(sf::RenderWindow* window, std::map<std::string, int>* supportedKeys, std::stack<State*>* states);
	virtual ~State();

	//Accessors
	const bool& getQuit() const;
	const bool getKeytime();

	//Functions
	void endState();
	void pauseState();
	void unPauseState();

	virtual void updateMousePositions(sf::View* view = NULL);
	virtual void updateKeytime(const float& dt);
	virtual void updateInput(const float& dt) = 0;

	virtual void update(
		const float& dt,
		std::queue<DataTransmitted>& toServer,
		std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLock,
		const appMode& mode,
		const bool& isConnected,
		const float wheelTicks) = 0;
	virtual void render(const float& dt = 0.f, const bool& isConnected = true) = 0;
};