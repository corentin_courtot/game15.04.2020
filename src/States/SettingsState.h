#pragma once

#include "DataTransmitted.h"
#include "Gui.h"
#include "State.h"

class SettingsState :
	public State
{
private:
	//Variables
	GraphicsSettings& gfxSettings;
	sf::Texture backgroundTexture;
	sf::RectangleShape background;
	sf::Font font;

	std::map<std::string, gui::TextButton*> buttons;
	std::map<std::string, gui::DropDownList*> dropDownLists;

	sf::Text optionsText;

	std::vector<sf::VideoMode> modes;

	//Functions
	void initVariables();
	void initBackground();
	void initFonts();
	void initKeybinds();
	void initGui();
	void initText();

public:
	SettingsState(sf::RenderWindow* window, GraphicsSettings& gfxSettings, std::map<std::string, int>* supportedKeys, std::stack<State*>* states);
	~SettingsState();

	//Accessors

	//Functions
	void updateInput(const float& dt);
	void updateGui(const float& dt);
	void update(
		const float& dt,
		std::queue<DataTransmitted>& toServer,
		std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLock,
		const appMode& mode,
		const bool& isConnected,
		const float wheelTicks);
	void renderGui(sf::RenderTarget& target);
	void render(const float& dt = 0.f, const bool& isConnected = true);
};
