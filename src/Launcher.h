#pragma once

#include "Client.h"
#include "DataTransmitted.h"
#include "Game.h"
#include "Server.h"

class Launcher
{
private:
	appMode mode;

	std::thread gameThread;
	std::thread clientThread;
	std::thread serverThread;

	std::mutex fromServerLock;

	std::queue<DataTransmitted> toServer;
	std::queue<DataTransmitted> fromServer;

	bool isConnected;

	unsigned short port;

	bool endClient;
	bool endServer;

public:
	Launcher(appMode mode = ALL);
	~Launcher();

	void runGame();
	void runClient();
	void runServer();
	int run();
};

Launcher::Launcher(appMode mode) :
	mode(mode),
	gameThread([this] { this->runGame(); }),
	clientThread([this] { this->runClient(); }),
	serverThread([this] { this->runServer(); }),
	isConnected(0),
	port(51003),
	endClient(0),
	endServer(0)
{
}

Launcher::~Launcher()
{
}

void Launcher::runGame()
{
	if (this->mode != SERVER_ONLY)
	{
		Game game;

		game.run(this->toServer, this->fromServer, this->fromServerLock, this->mode, this->isConnected);
	}
}

void Launcher::runClient()
{
	if (this->mode == ALL || this->mode == CLIENT)
	{
		Client client;

		client.run(&this->endClient, this->port, this->toServer, this->fromServer, this->fromServerLock, this->isConnected);
	}
}

void Launcher::runServer()
{
	if (this->mode == ALL || this->mode == SERVER_ONLY)
	{
		Server server;

		server.run(&this->endServer, this->port);
	}
}

int Launcher::run()
{
	gameThread.join(); // attend que gamethread soit terminé

	if (this->mode == SERVER_ONLY)
	{
		sf::sleep(sf::seconds(3));
		std::cout << "(Launcher) Press Enter to shut down server" << std::endl;
		std::cin.get();
	}

	this->endClient = true;
	this->endServer = true;

	clientThread.join();

	serverThread.join();

	return 0;
}