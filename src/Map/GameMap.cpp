
#include "GameMap.h"

void GameMap::initMap(time_t seed, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities)
{
	/* std::vector<std ::vector<uint8_t>> heightMap;
	std::vector<std ::vector<uint8_t>> humidityMap; */

	/*********************************************************************
	 * 					Perlin noise generation
	 *********************************************************************/
	auto startPerlin = std::chrono::steady_clock::now();
	myRand::srand(seed);
	PerlinNoise HeighLowResFeatures(this->mapSize.x, this->mapSize.y, 4);
	PerlinNoise HeightHighResFeatures(this->mapSize.x, this->mapSize.y, 7);

	HeightGrad = new Gradient(sf::Vector2i(this->mapSize.x / 2, this->mapSize.y / 2), 255, 0.5);

	//myRand::srand(rand());

	PerlinNoise HumidityLowResFeatures(this->mapSize.x, this->mapSize.y, 4);
	PerlinNoise HumidityHighResFeatures(this->mapSize.x, this->mapSize.y, 7);

	Gradient HumidityGrad(sf::Vector2i(this->mapSize.x / 2, this->mapSize.y / 2), 255, 0.5);
	auto endPerlin = std::chrono::steady_clock::now();
	std::cout << "\n(GameMap) Perlin gen : " << std::chrono::duration_cast<std::chrono::milliseconds>(endPerlin - startPerlin).count() << " ms" << std::endl;

	for (int x = 0; x < this->mapSize.x; x++)
	{
		this->tiles.push_back(std::vector<Tile>());
		for (int y = 0; y < this->mapSize.y; y++)
		{

			int height = (int)(0.6f * (HeighLowResFeatures.getNoise(coords) - 125.f)
				+ 0.2f * (HeightHighResFeatures.getNoise(coords) - 125.f)
				+ 0.48f * (HeightGrad->getGradient(coords, this->mapSize) + 1.f) * 255.f);

			if (height < 0)
			{
				height = 0;
			}
			else if (height > 255)
			{
				height = 255;
			}
			int humidity = (int)(1.f * HumidityLowResFeatures.getNoise(coords)
				+ 0.1f * HumidityHighResFeatures.getNoise(coords)
				+ 0.1f * HumidityGrad.getGradient(coords, this->mapSize) * 255.f);

			if (humidity < 0)
			{
				humidity = 0;
			}
			else if (humidity > 255)
			{
				humidity = 255;
			}
			this->tiles[x].push_back(Tile(x, y, (uint8_t)height, (uint8_t)humidity));
		}
	}

	/*********************************************************************
	 * 							FILTERS
	 ***********************************************************************/
	/* auto startFilter = std::chrono::steady_clock::now();
	//applyFilter(5, BLUR);
	//	applyFilter(3, BLUR);
	auto endFilter = std::chrono::steady_clock::now();
	std::cout << "Filters : " << std::chrono::duration_cast<std::chrono::milliseconds>(endFilter - startFilter).count() << " ms" << std::endl; */

	/**************************************************************************
	*						CHECK MAP
	***************************************************************************/
	auto startCheck = std::chrono::steady_clock::now();
	this->clusterExtraction();
	this->cleanSmallCluster();
	auto endCheck = std::chrono::steady_clock::now();
	std::cout << "(GameMap) Map check: " << std::chrono::duration_cast<std::chrono::milliseconds>(endCheck - startCheck).count() << " ms" << std::endl;

	/***************************************************************************
	 * 					ENTITY PLACEMENT
	 * *************************************************************************/
	auto startEntity = std::chrono::steady_clock::now();
	for (int y = 0; y < this->mapSize.y; y++) // y then x for better tree overlapping
	{
		for (int x = 0; x < this->mapSize.x; x++)
		{
			if (this->tiles[x][y].getBiome() == MONTAIN_FOREST)
			{
				if ((float)myRand::rand() / (float)myRand::randMax < (float)this->tiles[x][y].getHumidity() / (255.f * 20))
				{
					mapEntities.push_back(new Tree(sf::Vector2i(x, y), 0));
					this->tiles[x][y].setOccupied();
				}
			}
			else if (this->tiles[x][y].getBiome() == FOREST)
			{
				if ((float)myRand::rand() / (float)myRand::randMax < (float)this->tiles[x][y].getHumidity() / (255.f * 20))
				{
					mapEntities.push_back(new Tree(sf::Vector2i(x, y), 1));
					this->tiles[x][y].setOccupied();
				}
			}
			else if (this->tiles[x][y].getBiome() == GRASSLAND)
			{
				if ((float)myRand::rand() / (float)myRand::randMax < (float)this->tiles[x][y].getHumidity() / (255.f * 300))
				{
					mapEntities.push_back(new Tree(sf::Vector2i(x, y), 1));
					this->tiles[x][y].setOccupied();
					this->tiles[x][y + 1].setOccupied();
				}
			}
		}
	}
	auto endEntity = std::chrono::steady_clock::now();
	std::cout << "(GameMap) Entity placement : " << std::chrono::duration_cast<std::chrono::milliseconds>(endEntity - startEntity).count() << " ms" << std::endl;
}

void GameMap::heightMap_generation(unsigned int seed, std::vector<std ::vector<uint8_t>>& heightMap)
{
	myRand::srand(seed);
	PerlinNoise lowResFeatures(this->mapSize.x, this->mapSize.y, 4);
	myRand::srand(seed);
	PerlinNoise highResFeatures(this->mapSize.x, this->mapSize.y, 7);

	Gradient grad(sf::Vector2i(this->mapSize.x / 2, this->mapSize.y / 2), 255, 0.5);

	for (int x = 0; x < this->mapSize.x; x++)
	{
		heightMap.push_back(std::vector<uint8_t>());
		for (int y = 0; y < this->mapSize.y; y++)
		{
			int height = (int)(20.f + 1.f * lowResFeatures.getNoise(coords) + 0.3f * highResFeatures.getNoise(coords) + grad.getGradient(coords, this->mapSize) * 255.f);
			if (height < 0)
			{
				height = 0;
			}
			else if (height > 255)
			{
				height = 255;
			}

			heightMap[x].push_back((uint8_t)height);
		}
	}
}

void GameMap::humidityMap_generation(unsigned int seed, std::vector<std ::vector<uint8_t>>& humidityMap)
{
	srand(seed);
	PerlinNoise lowResFeatures(this->mapSize.x, this->mapSize.y, 4);
	srand(seed);
	PerlinNoise highResFeatures(this->mapSize.x, this->mapSize.y, 7);

	Gradient grad(sf::Vector2i(this->mapSize.x / 2, this->mapSize.y / 2), 255, 0.5);

	for (int x = 0; x < this->mapSize.x; x++)
	{
		humidityMap.push_back(std::vector<uint8_t>());
		for (int y = 0; y < this->mapSize.y; y++)
		{
			int humidity = (int)(20 + 1.f * lowResFeatures.getNoise(coords) + 0.2f * highResFeatures.getNoise(coords) + 0.25f * grad.getGradient(coords, this->mapSize) * 255.f);
			if (humidity < 0)
			{
				humidity = 0;
			}
			else if (humidity > 255)
			{
				humidity = 255;
			}

			humidityMap[x].push_back((uint8_t)humidity);
		}
	}
}

void GameMap::checkMap()
{
	for (int x = 1; x < this->mapSize.x - 1; x++)
	{
		for (int y = 1; y < this->mapSize.y - 1; y++)
		{
			std::vector<Tile*> sameNeighbors;

			//get_sameBiomeNeighbors(this->tiles[x][y], sameNeighbors);
			int nbNeighbors = get_nbSameBiomeNeighbors(this->tiles[x][y]);

			if (nbNeighbors <= 2)
			{
				std::vector<Tile*> neigbhors;
				getNeighbors(this->tiles[x][y], neigbhors);
				uint8_t averageHeight = 0;
				uint8_t averageHumidity = 0;

				for (unsigned int i = 0; i < (unsigned int)neigbhors.size(); i++)
				{
					averageHeight += (uint8_t)((float)neigbhors[i]->getHeight() / neigbhors.size());
					averageHumidity += (uint8_t)((float)neigbhors[i]->getHumidity() / neigbhors.size());
				}
				//this->tiles[x][y] = Tile(x, y, averageHeight);
				this->tiles[x][y].setHeight(averageHeight);
				this->tiles[x][y].setHumidity(averageHumidity);
				this->tiles[x][y].updateBiome();
			}
		}
	}
}

GameMap::GameMap(sf::Vector2i tile_size, unsigned int width, unsigned int height, time_t seed, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities) :
	TileMap(width, height)
{
	auto startMapGen = std::chrono::steady_clock::now();
	this->initMap(seed, entities, mapEntities);
	auto endMapGen = std::chrono::steady_clock::now();

	std::cout << "(GameMap) Time to generate the map : " << std::chrono::duration_cast<std::chrono::milliseconds>(endMapGen - startMapGen).count() << " ms\n"
			  << std::endl;

	if (!this->tileTexture.loadFromFile("Ressources/Images/Sprites/Tile/tileTexture.png"))
		throw "ERROR::GameMap::FAILED_TO_LOAD_TILE_TEXTURE";

	// on redimensionne le tableau de vertex pour qu'il puisse contenir tout le niveau
	this->m_vertices.setPrimitiveType(sf::Quads);
	this->m_vertices.resize(this->mapSize.x * this->mapSize.y * 4);

	this->renderedVerticesSize = sf::Vector2i((int)(maxViewSize.x / tileSize.x + 2.f), (int)(maxViewSize.y / tileSize.y + 2.f));
	this->renderedVertices.setPrimitiveType(sf::Quads);
	this->renderedVertices.resize(renderedVerticesSize.x * renderedVerticesSize.y * 4);

	// on remplit le tableau de vertex, avec un quad par tuile
	int margin = 5; // marge dans les textures des tiles
	sf::Vector2i tileSizeWithMargin = tile_size + sf::Vector2i(2 * margin, 2 * margin);
	for (int i = 0; i < this->mapSize.x; ++i)
	{
		for (int j = 0; j < this->mapSize.y; ++j)
		{
			// on r�cup�re le num�ro de tuile courant
			int tileNumber = (int)this->tiles[i][j].getGroundType();

			// on en d�duit sa position dans la texture du tileset
			int tu = tileNumber % (tileTexture.getSize().x / tileSize.x);
			int tv = myRand::rand() % 3 + tileNumber / (tileTexture.getSize().y / tileSize.y);

			// on r�cup�re un pointeur vers le quad � d�finir dans le tableau de vertex
			sf::Vertex* quad = &m_vertices[(i + j * this->mapSize.x) * 4];

			// on d�finit ses quatre coins
			quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
			quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
			quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
			quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

			// on d�finit ses quatre coordonn�es de texture
			quad[0].texCoords = sf::Vector2f(margin + tu * tileSizeWithMargin.x,
				margin + tv * tileSizeWithMargin.y);
			quad[1].texCoords = sf::Vector2f(-margin + (tu + 1) * tileSizeWithMargin.x,
				margin + tv * tileSizeWithMargin.y);
			quad[2].texCoords = sf::Vector2f(-margin + (tu + 1) * tileSizeWithMargin.x,
				-margin + (tv + 1) * tileSizeWithMargin.y);
			quad[3].texCoords = sf::Vector2f(margin + tu * tileSizeWithMargin.x,
				-margin + (tv + 1) * tileSizeWithMargin.y);
		}
	}
}

GameMap::~GameMap()
{
}

bool GameMap::load(const std::string& tileset, const int* tiles)
{
	// on charge la texture du tileset
	if (!this->tileTexture.loadFromFile(tileset))
		return false;

	// on redimensionne le tableau de vertex pour qu'il puisse contenir tout le niveau
	this->m_vertices.setPrimitiveType(sf::Quads);
	this->m_vertices.resize(mapSize.x * this->mapSize.y * 4);

	// on remplit le tableau de vertex, avec un quad par tuile
	for (int i = 0; i < this->mapSize.x; ++i)
		for (int j = 0; j < this->mapSize.y; ++j)
		{
			// on r�cup�re le num�ro de tuile courant
			int tileNumber = tiles[i + j * this->mapSize.x];

			// on en d�duit sa position dans la texture du tileset
			int tu = tileNumber % (tileTexture.getSize().x / tileSize.x);
			int tv = tileNumber / (tileTexture.getSize().x / tileSize.x);

			// on r�cup�re un pointeur vers le quad � d�finir dans le tableau de vertex
			sf::Vertex* quad = &m_vertices[(i + j * this->mapSize.x) * 4];

			// on d�finit ses quatre coins
			quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
			quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
			quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
			quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

			// on d�finit ses quatre coordonn�es de texture
			quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
			quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
			quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
			quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
		}

	return true;
}

void GameMap::render(sf::RenderTarget& target, const sf::Vector2i viewCenter)
{
	// calcul de la position du point en haut à gauche de la vue par rapport à la map
	sf::Vector2i referenceOrigin = sf::Vector2i(
		viewCenter.x - (int)(this->renderedVerticesSize.x * 0.5f),
		viewCenter.y - (int)(this->renderedVerticesSize.y * 0.5f));

	for (int i = 0; i < this->renderedVerticesSize.x; i++)
	{
		for (int j = 0; j < this->renderedVerticesSize.y; j++)
		{
			sf::Vertex* renderedQuad = &this->renderedVertices[(i + j * renderedVerticesSize.x) * 4];

			if (referenceOrigin.x + i > 0 && referenceOrigin.x + i < this->mapSize.x
				&& referenceOrigin.y + j > 0 && referenceOrigin.y + j < this->mapSize.y)
			{
				sf::Vertex* referenceQuad =
					&this->m_vertices[(referenceOrigin.x + i + (referenceOrigin.y + j) * this->mapSize.x) * 4];

				// on d�finit ses quatre coins
				renderedQuad[0].position = referenceQuad[0].position;
				renderedQuad[1].position = referenceQuad[1].position;
				renderedQuad[2].position = referenceQuad[2].position;
				renderedQuad[3].position = referenceQuad[3].position;

				// on d�finit ses quatre coordonn�es de texture
				renderedQuad[0].texCoords = referenceQuad[0].texCoords;
				renderedQuad[1].texCoords = referenceQuad[1].texCoords;
				renderedQuad[2].texCoords = referenceQuad[2].texCoords;
				renderedQuad[3].texCoords = referenceQuad[3].texCoords;
			}
			else // si on est hors de la map : noir
			{
				renderedQuad[0].position = sf::Vector2f(0.f, 0.f);
				renderedQuad[1].position = sf::Vector2f(0.f, 0.f);
				renderedQuad[2].position = sf::Vector2f(0.f, 0.f);
				renderedQuad[3].position = sf::Vector2f(0.f, 0.f);
			}
		}
	}

	target.draw(this->renderedVertices, &this->tileTexture);
}