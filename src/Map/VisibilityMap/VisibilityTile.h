#pragma once

#include "GeneralFunctions.h"

enum VisibilityTileState
{
	NEVER_SEEN = 0,
	ON_SIGHT,
	SEEN_BEFORE
};

class VisibilityTile
{
private:
	const unsigned int indX;
	const unsigned int indY;

public:
	VisibilityTileState visibilityState;

	VisibilityTile(unsigned int indX, unsigned int indY);
	~VisibilityTile();

	void update(float distToInterestPoint);
};