#include "VisibilityMap.h"

void VisibilityMap::initVisibilityMap()
{
	for (int x = 0; x < this->mapSize.x; x++)
	{
		this->visibilityTiles.push_back(std::vector<VisibilityTile>());
		for (int y = 0; y < this->mapSize.y; y++)
		{
			this->visibilityTiles[x].push_back(VisibilityTile(x, y));
		}
	}
}

void VisibilityMap::changeTileState(sf::Vector2i pos, VisibilityTileState newState)
{
	// mise a jour de l'etat de la tile
	this->visibilityTiles[pos.x][pos.y].visibilityState = newState;

	// on r�cup�re le num�ro de tuile courant
	int tileNumber = (int)this->visibilityTiles[pos.x][pos.y].visibilityState;

	// on en d�duit sa position dans la texture du tileset
	int tu = tileNumber % (tileTexture.getSize().x / tileSize.x);
	int tv = tileNumber / (tileTexture.getSize().y / tileSize.y);

	int margin = 5; // marge dans les textures des tiles
	sf::Vector2i tileSizeWithMargin = tileSize + sf::Vector2i(2 * margin, 2 * margin);

	// on r�cup�re un pointeur vers le quad � d�finir dans le tableau de vertex
	sf::Vertex* quad = &vertices[(pos.x + pos.y * mapSize.x) * 4];

	// on d�finit ses quatre coordonn�es de texture
	quad[0].texCoords = sf::Vector2f(margin + tu * tileSizeWithMargin.x,
		margin + tv * tileSizeWithMargin.y);
	quad[1].texCoords = sf::Vector2f(-margin + (tu + 1) * tileSizeWithMargin.x,
		margin + tv * tileSizeWithMargin.y);
	quad[2].texCoords = sf::Vector2f(-margin + (tu + 1) * tileSizeWithMargin.x,
		-margin + (tv + 1) * tileSizeWithMargin.y);
	quad[3].texCoords = sf::Vector2f(margin + tu * tileSizeWithMargin.x,
		-margin + (tv + 1) * tileSizeWithMargin.y);
}

void VisibilityMap::updateOnEdgeTileTexture(sf::Vector2i pos, float ratio)
{
	// on r�cup�re le num�ro de tuile courant
	int tileNumber = (int)this->visibilityTiles[pos.x][pos.y].visibilityState;

	int yTexture = nbTextures - std::min((int)((ratio - 1) * nbTextures * 6), nbTextures);

	// on en d�duit sa position dans la texture du tileset
	int tu = tileNumber % (tileTexture.getSize().x / tileSize.x);
	int tv = yTexture + tileNumber / (tileTexture.getSize().y / tileSize.y);

	int margin = 5; // marge dans les textures des tiles
	sf::Vector2i tileSizeWithMargin = tileSize + sf::Vector2i(2 * margin, 2 * margin);

	// on r�cup�re un pointeur vers le quad � d�finir dans le tableau de vertex
	sf::Vertex* quad = &vertices[(pos.x + pos.y * mapSize.x) * 4];

	// on d�finit ses quatre coordonn�es de texture
	quad[0].texCoords = sf::Vector2f(margin + tu * tileSizeWithMargin.x,
		margin + tv * tileSizeWithMargin.y);
	quad[1].texCoords = sf::Vector2f(-margin + (tu + 1) * tileSizeWithMargin.x,
		margin + tv * tileSizeWithMargin.y);
	quad[2].texCoords = sf::Vector2f(-margin + (tu + 1) * tileSizeWithMargin.x,
		-margin + (tv + 1) * tileSizeWithMargin.y);
	quad[3].texCoords = sf::Vector2f(margin + tu * tileSizeWithMargin.x,
		-margin + (tv + 1) * tileSizeWithMargin.y);
}

VisibilityMap::VisibilityMap(sf::Vector2i mapSize) :
	mapSize(mapSize),
	nbTextures(7)
{
	this->initVisibilityMap();

	if (!this->tileTexture.loadFromFile("Ressources/Images/Sprites/Tile/visibilityTileTexture.png"))
		throw "ERROR::GameMap::FAILED_TO_LOAD_TILE_TEXTURE";

	// on redimensionne le tableau de vertex pour qu'il puisse contenir tout le niveau
	this->vertices.setPrimitiveType(sf::Quads);
	this->vertices.resize(this->mapSize.x * this->mapSize.y * 4);

	this->renderedVerticesSize = sf::Vector2i((int)(maxViewSize.x / tileSize.x + 2.f), (int)(maxViewSize.y / tileSize.y + 2.f));
	this->renderedVertices.setPrimitiveType(sf::Quads);
	this->renderedVertices.resize(renderedVerticesSize.x * renderedVerticesSize.y * 4);

	// on remplit le tableau de vertex, avec un quad par tuile
	int margin = 5; // marge das les textures des tiles
	sf::Vector2i tileSizeWithMargin = tileSize + sf::Vector2i(2 * margin, 2 * margin);
	for (int i = 0; i < mapSize.x; ++i)
	{
		for (int j = 0; j < mapSize.y; ++j)
		{
			// on r�cup�re le num�ro de tuile courant
			int tileNumber = (int)this->visibilityTiles[i][j].visibilityState;

			// on en d�duit sa position dans la texture du tileset
			int tu = tileNumber % (tileTexture.getSize().x / tileSize.x);
			int tv = tileNumber / (tileTexture.getSize().y / tileSize.y);

			// on r�cup�re un pointeur vers le quad � d�finir dans le tableau de vertex
			sf::Vertex* quad = &vertices[(i + j * mapSize.x) * 4];

			// on d�finit ses quatre coins
			quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
			quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
			quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
			quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

			// on d�finit ses quatre coordonn�es de texture
			quad[0].texCoords = sf::Vector2f(margin + tu * tileSizeWithMargin.x,
				margin + tv * tileSizeWithMargin.y);
			quad[1].texCoords = sf::Vector2f(-margin + (tu + 1) * tileSizeWithMargin.x,
				margin + tv * tileSizeWithMargin.y);
			quad[2].texCoords = sf::Vector2f(-margin + (tu + 1) * tileSizeWithMargin.x,
				-margin + (tv + 1) * tileSizeWithMargin.y);
			quad[3].texCoords = sf::Vector2f(margin + tu * tileSizeWithMargin.x,
				-margin + (tv + 1) * tileSizeWithMargin.y);
		}
	}
}

VisibilityMap::~VisibilityMap()
{
}

void VisibilityMap::updateVisibility(const sf::Vector2f& tilePos, const int& viewRange)
{
	sf::Vector2i indPos = vector2fToIndTile(tilePos);
	for (int i = -pixelDistToTileDist(viewRange) - 2; i < pixelDistToTileDist(viewRange) + 2; ++i)
	{
		for (int j = -pixelDistToTileDist(viewRange) - 2; j < pixelDistToTileDist(viewRange) + 2; ++j)
		{
			if (indPos.x + i >= 0 && indPos.x + i < mapSize.x
				&& indPos.y + j >= 0 && indPos.y + j < mapSize.y) // if the tile is within map
			{
				if (distSquared(IndTileToVector2f(sf::Vector2i(i, j))) < viewRange * viewRange)
				{
					this->changeTileState(indPos + sf::Vector2i(i, j), ON_SIGHT);
				}
				else if (distSquared(IndTileToVector2f(sf::Vector2i(i, j))) / (viewRange * viewRange) < 1.5f)
				{
					if (this->visibilityTiles[indPos.x + i][indPos.y + j].visibilityState == ON_SIGHT)
					{
						this->changeTileState(indPos + sf::Vector2i(i, j), SEEN_BEFORE);
					}
					updateOnEdgeTileTexture(indPos + sf::Vector2i(i, j),
						distSquared(IndTileToVector2f(sf::Vector2i(i, j))) / (viewRange * viewRange));
				}
			}
		}
	}
}

void VisibilityMap::render(sf::RenderTarget& target, const sf::Vector2i viewCenter)
{
	// code idem que dans gameMap.cpp
	sf::Vector2i referenceOrigin = sf::Vector2i(
		viewCenter.x - (int)(this->renderedVerticesSize.x * 0.5f),
		viewCenter.y - (int)(this->renderedVerticesSize.y * 0.5f));

	for (int i = 0; i < this->renderedVerticesSize.x; i++)
	{
		for (int j = 0; j < this->renderedVerticesSize.y; j++)
		{
			sf::Vertex* renderedQuad = &this->renderedVertices[(i + j * renderedVerticesSize.x) * 4];

			if (referenceOrigin.x + i > 0 && referenceOrigin.x + i < this->mapSize.x
				&& referenceOrigin.y + j > 0 && referenceOrigin.y + j < this->mapSize.y)
			{
				sf::Vertex* referenceQuad =
					&this->vertices[(referenceOrigin.x + i + (referenceOrigin.y + j) * this->mapSize.x) * 4];

				// on d�finit ses quatre coins
				renderedQuad[0].position = referenceQuad[0].position;
				renderedQuad[1].position = referenceQuad[1].position;
				renderedQuad[2].position = referenceQuad[2].position;
				renderedQuad[3].position = referenceQuad[3].position;

				// on d�finit ses quatre coordonn�es de texture
				renderedQuad[0].texCoords = referenceQuad[0].texCoords;
				renderedQuad[1].texCoords = referenceQuad[1].texCoords;
				renderedQuad[2].texCoords = referenceQuad[2].texCoords;
				renderedQuad[3].texCoords = referenceQuad[3].texCoords;
			}
			else
			{
				renderedQuad[0].position = sf::Vector2f(0.f, 0.f);
				renderedQuad[1].position = sf::Vector2f(0.f, 0.f);
				renderedQuad[2].position = sf::Vector2f(0.f, 0.f);
				renderedQuad[3].position = sf::Vector2f(0.f, 0.f);
			}
		}
	}

	target.draw(this->renderedVertices, &this->tileTexture);
}