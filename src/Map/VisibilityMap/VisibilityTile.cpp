#include "VisibilityTile.h"

VisibilityTile::VisibilityTile(unsigned int indX, unsigned int indY) :
	indX(indX),
	indY(indY),
	visibilityState(NEVER_SEEN)
{
}

VisibilityTile::~VisibilityTile()
{
}
