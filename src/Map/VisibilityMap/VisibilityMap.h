#pragma once

#include "GeneralFunctions.h"
#include "VisibilityTile.h"

class VisibilityMap
{
private:
	sf::Vector2i mapSize;
	sf::Texture tileTexture;

	// Vertax array with all information for rendering
	sf::VertexArray vertices;
	// Vertex array that will actually be render. More optimized than rendering all vertices
	sf::Vector2i renderedVerticesSize;
	sf::VertexArray renderedVertices;

	std::vector<std::vector<VisibilityTile>> visibilityTiles;

	const int nbTextures;

	void initVisibilityMap();
	void changeTileState(sf::Vector2i tilePos, VisibilityTileState newState);
	// updates tile texture in order to have fading mist effect
	void updateOnEdgeTileTexture(sf::Vector2i pos, float ratio); // ratio is distToTile/viewRange (> 1)

public:
	VisibilityMap(sf::Vector2i mapSize);
	~VisibilityMap();

	void updateVisibility(const sf::Vector2f& pos, const int& viewRange);

	void render(sf::RenderTarget& target, const sf::Vector2i viewCenter);
};