#include "DisjSet.h"

DisjSet::DisjSet(/* args */)
{
	this->parent.push_back(0);
}
DisjSet::~DisjSet()
{
}

unsigned int DisjSet::find(unsigned int x)
{
	if (this->parent[x] != x)
	{
		this->parent[x] = find(this->parent[x]);
	}
	return this->parent[x];
}

void DisjSet::Union(unsigned int x, unsigned int y)
{
	unsigned int xSet = find(x);
	unsigned int ySet = find(y);

	if (xSet == ySet)
		return;

	if (this->parent[xSet] < this->parent[ySet])
		this->parent[ySet] = xSet;

	else
		this->parent[xSet] = ySet;
}

void DisjSet::addSet()
{
	this->parent.push_back(this->parent.size());
}

unsigned int DisjSet::get_nbSet()
{
	return (unsigned int)this->parent.size();
}