
#include "Tile.h"

Tile::Tile(unsigned int ind_x, unsigned int ind_y, uint8_t height, uint8_t humidity) :
	indX(ind_x),
	indY(ind_y),
	height(height),
	humidity(humidity)
{
	this->moveSpeedModifier = 1.f;
	this->label = 0;
	this->clusterID = 0;
	this->isOcupied = false;
	this->updateBiome();
}

Tile::~Tile()
{
}

float Tile::getMoveSpeed() const
{
	return this->moveSpeedModifier;
}

void Tile::updateBiome()
{
	if (true)
	{
		if (this->height < 40)
		{
			this->biome = OCEAN;
		}
		else if (this->height < 75)
		{
			this->biome = BEACH;
		}
		else if (this->height < 90)
		{
			if (this->humidity < 70)
			{
				this->biome = DESERT;
			}
			else if (this->humidity < 150)
			{
				this->biome = GRASSLAND;
			}
			else
			{
				this->biome = FOREST;
			}
		}

		else if (this->height < 140)
		{
			if (this->humidity < 90)
			{
				this->groundType = SAND;
				this->biome = DESERT;
			}
			else if (this->humidity < 150)
			{
				this->biome = GRASSLAND;
			}
			else if (this->humidity < 212)
			{
				this->biome = FOREST;
			}
			else
			{
				this->biome = LAKE;
			}
		}

		else if (this->height < 180)
		{
			if (this->humidity < 90)
			{
				this->biome = DESERT;
			}
			else if (this->humidity < 150)
			{
				this->biome = GRASSLAND;
			}
			else if (this->humidity < 212)
			{
				this->biome = MONTAIN_FOREST;
			}
			else
			{
				this->biome = LAKE;
			}
		}

		else if (this->height < 210)
		{
			if (this->humidity < 90)
			{
				this->biome = DESERT;
			}
			else if (this->humidity < 125)
			{
				this->biome = GRASSLAND;
			}
			else
			{
				this->biome = MONTAIN_FOREST;
			}
		}
		else
		{
			this->biome = MOUNTAIN;
		}
	}
	else
	{
		this->biome = BIOME_DEBUG;
	}

	switch (this->biome)
	{
		case OCEAN:
			this->groundType = WATER;
			this->moveSpeedModifier = 0.1f;
			break;
		case LAKE:
			this->groundType = WATER;
			this->moveSpeedModifier = 0.1f;
			break;
		case BEACH:
			this->groundType = SAND;
			this->moveSpeedModifier = 1.f;
			break;
		case DESERT:
			this->groundType = SAND;
			this->moveSpeedModifier = 1.f;
			break;
		case GRASSLAND:
			this->groundType = GRASS;
			this->moveSpeedModifier = 1.f;
			break;
		case FOREST:
			this->groundType = SOIL;
			this->moveSpeedModifier = 0.8f;
			break;
		case MONTAIN_FOREST:
			this->groundType = SOIL;
			this->moveSpeedModifier = 0.8f;
			break;
		case MOUNTAIN:
			this->groundType = GRAVEL;
			this->moveSpeedModifier = 0.5f;
			break;
		case BIOME_DEBUG:
			this->groundType = DEBUG;
			this->moveSpeedModifier = 1.f;
			break;

		default:
			break;
	}
}

/**
 * Accessors
 */

sf::Vector2i Tile::getInd() const
{
	return sf::Vector2i(indX, indY);
}

sf::Vector2i Tile::getPos() const
{
	return sf::Vector2i((int)((indX + 0.5f) * tileSize.x), (int)((indY + 0.5f) * tileSize.y));
}

ground_type Tile::getGroundType() const
{
	return groundType;
}

void Tile::setGroundType(const ground_type& _groundType)
{
	this->groundType = _groundType;
}

Biome Tile::getBiome() const
{
	return biome;
}
void Tile::setBiome(const Biome& _biome)
{
	this->biome = _biome;
}

uint8_t Tile::getHeight() const
{
	return height;
}

void Tile::setHeight(const uint8_t& height)
{
	this->height = height;
}

uint8_t Tile::getHumidity() const
{
	return this->humidity;
}
void Tile::setHumidity(uint8_t humidity)
{
	this->humidity = humidity;
}

unsigned int Tile::getLabel() const
{
	return this->label;
}

void Tile::setLabel(const unsigned int label)
{
	this->label = label;
}

unsigned int Tile::getClusterID() const
{
	return this->clusterID;
}
void Tile::setClusterID(const unsigned int clusterID)
{
	this->clusterID = clusterID;
}

void Tile::setOccupied()
{
	this->isOcupied = true;
}

bool Tile::is_occupied() const
{
	/* 	if (!this->isOcupied)
		std::cout << "(Tile)is occupied" << std::endl; */
	return this->isOcupied;
}
/**
 * Operators
 *
 */

Tile& Tile::operator=(const Tile& copy)
{
	/*this->indX = copy.indX;
	this->indY = copy.indY;*/

	this->biome = copy.biome;
	this->groundType = copy.groundType;
	this->moveSpeedModifier = copy.moveSpeedModifier;

	this->height = copy.height;
	this->humidity = copy.humidity;

	this->label = copy.label;
	this->clusterID = copy.clusterID;

	return *this;
}
