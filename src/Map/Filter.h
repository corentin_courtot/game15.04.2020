#pragma once

enum FilterType
{
	BLUR = 0
};

class Filter
{
private:
	uint8_t size;
	FilterType filtertype;

	float** kernel;

	void generateBlurKernel();

public:
	Filter(uint8_t size, FilterType filterType);
	~Filter();

	float** getKernel() const
	{
		return kernel;
	}

	uint8_t getSize() const
	{
		return size;
	}
};
