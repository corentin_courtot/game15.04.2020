#pragma once

#include "GeneralFunctions.h"

class PerlinNoise
{
private:
	int nOutputWidth = 300;
	int nOutputHeight = 200;
	int scale = 7;
	float* fNoiseSeed2D = nullptr;
	uint8_t* fPerlinNoise2D = nullptr;
	float fScalingBias = 0.2f;

public:
	/**
 * @brief create a Perlin Noise 2D array
 *
 * @param width
 * @param height
 * @param scale : from 1 to 7, affects the size of the zones
 */
	PerlinNoise(int width, int height, int scale);
	~PerlinNoise();

	void PerlinNoise2D(int nWidth, int nHeight, float* fSeed, int nOctaves, float fBias, uint8_t* fOutput);

	float getNoise(int x, int y);
};