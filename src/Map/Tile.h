#pragma once

#include "GeneralFunctions.h"

enum ground_type
{
	SOIL = 0,
	GRAVEL,
	SAND,
	WATER,
	GRASS,
	RANDOM,
	DEBUG
};

enum Biome
{
	OCEAN = 0,
	LAKE,
	BEACH,
	DESERT,
	GRASSLAND,
	FOREST,
	MONTAIN_FOREST,
	MOUNTAIN,
	BIOME_DEBUG
};

class Tile
{
private:
	unsigned int indX;
	unsigned int indY;

	Biome biome;
	ground_type groundType;
	float moveSpeedModifier;

	uint8_t height;
	uint8_t humidity;

	unsigned int label;
	unsigned int clusterID;

	bool isOcupied;

protected:
public:
	Tile(unsigned int ind_x, unsigned int ind_y, uint8_t height, uint8_t humidity);
	~Tile();

	void updateBiome();

	/************************************************************
	 * 						Acessors
	 ***********************************************************/

	sf::Vector2i getInd() const;
	sf::Vector2i getPos() const;

	float getMoveSpeed() const;

	ground_type getGroundType() const;
	void setGroundType(const ground_type& set_GroundType);

	Biome getBiome() const;
	void setBiome(const Biome& _biome);

	uint8_t getHeight() const;
	void setHeight(const uint8_t& height);

	uint8_t getHumidity() const;
	void setHumidity(uint8_t humidity);

	unsigned int getLabel() const;
	void setLabel(const unsigned int label);

	unsigned int getClusterID() const;
	void setClusterID(const unsigned int clusterID);

	void setOccupied();
	bool is_occupied() const;

	/***********************************************************
	 * 						Operators
	 *********************************************************/
	Tile& operator=(const Tile& copy);
};
