#pragma once

class DisjSet
{
private:
	std::vector<unsigned int> parent;

public:
	DisjSet(/* args */);
	~DisjSet();

	unsigned int find(unsigned int x);

	void Union(unsigned int x, unsigned int y);

	void addSet();

	unsigned int get_nbSet();
};
