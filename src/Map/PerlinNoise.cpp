#include "PerlinNoise.h"

PerlinNoise::PerlinNoise(int width, int height, int scale) :
	nOutputWidth(width),
	nOutputHeight(height),
	scale(scale)
{
	fNoiseSeed2D = new float[nOutputWidth * nOutputHeight];
	fPerlinNoise2D = new uint8_t[nOutputWidth * nOutputHeight];
	fScalingBias = 0.2f;

	//srand(time(NULL));

	for (int i = 0; i < nOutputWidth * nOutputHeight; i++)
		fNoiseSeed2D[i] = (float)myRand::rand() / (float)myRand::randMax;

	if (scale < 1 || scale > 8)
		scale = 8;

	PerlinNoise2D(nOutputWidth, nOutputHeight, fNoiseSeed2D, scale, fScalingBias, fPerlinNoise2D);
}

PerlinNoise::~PerlinNoise()
{
	delete fPerlinNoise2D;
	delete fNoiseSeed2D;
}

void PerlinNoise::PerlinNoise2D(int nWidth, int nHeight, float* fSeed, int nOctaves, float fBias, uint8_t* fOutput)
{
	for (int x = 0; x < nWidth; x++)
	{
		for (int y = 0; y < nHeight; y++)
		{
			float fNoise = 0.0f;
			float fScaleAcc = 0.0f;
			float fScale = 1.0f;

			for (int o = 0; o < nOctaves; o++)
			{
				int nPitch = nWidth >> o;
				int nSampleX1 = (x / nPitch) * nPitch;
				int nSampleY1 = (y / nPitch) * nPitch;

				int nSampleX2 = (nSampleX1 + nPitch) % nWidth;
				int nSampleY2 = (nSampleY1 + nPitch) % nHeight;

				float fBlendX = (float)(x - nSampleX1) / (float)nPitch;
				float fBlendY = (float)(y - nSampleY1) / (float)nPitch;

				float fSampleT = 0.f;
				float fSampleB = 0.f;

				fSampleT = (1.0f - fBlendX) * fSeed[nSampleY1 * nWidth + nSampleX1] + fBlendX * fSeed[nSampleY1 * nWidth + nSampleX2];
				fSampleB = (1.0f - fBlendX) * fSeed[nSampleY2 * nWidth + nSampleX1] + fBlendX * fSeed[nSampleY2 * nWidth + nSampleX2];

				fScaleAcc += fScale;
				fNoise += (fBlendY * (fSampleB - fSampleT) + fSampleT) * fScale;
				fScale = fScale / fBias;
			}
			// Scale to seed range
			fOutput[y * nWidth + x] = (uint8_t)((fNoise / fScaleAcc) * 255);
		}
	}
}

float PerlinNoise::getNoise(int x, int y)
{
	return fPerlinNoise2D[y * nOutputWidth + x];
}