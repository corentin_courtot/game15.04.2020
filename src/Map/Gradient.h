#pragma once

class Gradient
{
private:
	sf::Vector2i origin;
	int max;
	int coeff;
	float lMax;

public:
	Gradient(sf::Vector2i origin, int max, float coeff);
	~Gradient()
	{}

	float getGradient(int x, int y, sf::Vector2i mapSize);
};
