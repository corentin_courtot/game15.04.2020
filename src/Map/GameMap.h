#pragma once

#include "Gradient.h"
#include "PerlinNoise.h"
#include "TileMap.h"
#include "Tree.h"

#define coords x, y

class GameMap : public TileMap
{
private:
	sf::Texture tileTexture;
	// Vertax array with all information for rendering
	sf::VertexArray m_vertices;
	// Vertex array that will actually be render. More optimized than rendering all vertices
	sf::Vector2i renderedVerticesSize;
	sf::VertexArray renderedVertices;

	void heightMap_generation(unsigned int seed, std::vector<std ::vector<uint8_t>>& heighMap);
	void humidityMap_generation(unsigned int seed, std::vector<std ::vector<uint8_t>>& humidityhMap);

public:
	//Initializer functions
	void initMap(time_t seed, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities);
	void checkMap();
	Gradient* HeightGrad;
	sf::Vector2i getTileSize() const
	{
		return tileSize;
	}

	GameMap(sf::Vector2i tile_size, unsigned int width, unsigned int height, time_t seed, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities);
	~GameMap();

	bool load(const std::string& tileset, const int* tiles);
	void render(sf::RenderTarget& target, const sf::Vector2i viewCenter);
};
