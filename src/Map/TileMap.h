#pragma once

#include "DisjSet.h"
#include "Filter.h"
#include "Tile.h"

#define CLUSTER_MIN_SIZE 50
class TileMap
{
private:
protected:
	sf::Vector2i mapSize;

	std::vector<std::vector<Tile*>> clusters;

public:
	std::vector<std::vector<Tile>> tiles; //TODO: change to private
	TileMap(unsigned int width, unsigned int height);
	~TileMap();

	void get_sameBiomeNeighbors(const Tile& tile, std::vector<Tile*>& sameBiomeNeighbors);
	int get_nbSameBiomeNeighbors(const Tile& tile);

	void getNeighbors(const Tile& tile, std::vector<Tile*>& neighbors);

	void applyFilter(uint8_t size, FilterType filter);

	void clusterExtraction();

	void cleanSmallCluster();

	/*************************************************************************
 * 			Accessors
 *************************************************************************/
	sf::Vector2i getMapSize() const;
	float getMoveSpeed(sf::Vector2f pos) const;
	ground_type getGroundType(sf::Vector2f pos) const;
};
