#include "TileMap.h"

TileMap::TileMap(unsigned int width, unsigned int height) :
	mapSize(sf::Vector2i(width, height))
{
}

TileMap::~TileMap()
{
}

void TileMap::get_sameBiomeNeighbors(const Tile& tile, std::vector<Tile*>& sameBiomeNeighbors)
{
	for (int x = tile.getInd().x - 1; x <= tile.getInd().x + 1; x++)
	{
		for (int y = tile.getInd().y - 1; y <= tile.getInd().y + 1; y++)
		{
			if (!(x == tile.getInd().x && y == tile.getInd().y))
			{
				if (tile.getBiome() == this->tiles[x][y].getBiome())
				{
					sameBiomeNeighbors.push_back(&this->tiles[x][y]);
				}
			}
		}
	}
}

int TileMap::get_nbSameBiomeNeighbors(const Tile& tile)
{
	int nbSameBiome = 0;
	for (int x = tile.getInd().x - 1; x <= tile.getInd().x + 1; x++)
	{
		for (int y = tile.getInd().y - 1; y <= tile.getInd().y + 1; y++)
		{
			if (!(x == tile.getInd().x && y == tile.getInd().y))
			{
				if (tile.getBiome() == this->tiles[x][y].getBiome())
				{
					nbSameBiome++;
				}
			}
		}
	}
	return nbSameBiome;
}

void TileMap::getNeighbors(const Tile& tile, std::vector<Tile*>& neighbors)
{
	for (int x = tile.getInd().x - 1; x <= tile.getInd().x + 1; x++)
	{
		for (int y = tile.getInd().y - 1; y <= tile.getInd().y + 1; y++)
		{
			if (!(x == tile.getInd().x && y == tile.getInd().y))
			{
				neighbors.push_back(&this->tiles[x][y]);
			}
		}
	}
}

void TileMap::applyFilter(uint8_t size, FilterType filter_type)
{
	float** kernel;
	Filter filter(size, filter_type);

	kernel = filter.getKernel();

	int offset = size / 2;

	for (int x = offset; x < mapSize.x - offset; x++)
	{
		for (int y = offset; y < mapSize.y - offset; y++)
		{
			int k = 0;
			uint8_t height = 0;
			uint8_t humidity = 0;
			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < size; j++)
				{
					height += /* (uint8_t) */ ((float)this->tiles[x - offset + i][y - offset + j].getHeight() * kernel[i][j]);
					humidity += (uint8_t)((float)this->tiles[x - offset + i][y - offset + j].getHumidity() * kernel[i][j]);
					k++;
				}
			}
			//this->tiles[x][y] = Tile(x, y, height);
			this->tiles[x][y].setHeight((uint8_t)height);
			this->tiles[x][y].setHumidity(humidity);
			this->tiles[x][y].updateBiome();
		}
	}
}

void TileMap::clusterExtraction()
{
	/**
	 * @brief Two pass connected component algorithm implementation
	 * pseudo code at : https://en.wikipedia.org/wiki/Connected-component_labeling
	 * https://www.geeksforgeeks.org/disjoint-set-data-structures/
	 *
	 */
	DisjSet eqLabel;

	for (int x = 0; x < mapSize.x; x++)
	{
		for (int y = 0; y < mapSize.y; y++)
		{

			if (x != 0 && y != 0)
			{

				if (this->tiles[x - 1][y].getBiome() == this->tiles[x][y].getBiome())
				{
					this->tiles[x][y].setLabel(this->tiles[x - 1][y].getLabel());

					if (this->tiles[x][y - 1].getBiome() == this->tiles[x][y].getBiome() && this->tiles[x - 1][y].getLabel() != this->tiles[x][y - 1].getLabel())
					{
						this->tiles[x][y].setLabel(std::min(this->tiles[x - 1][y].getLabel(), this->tiles[x][y - 1].getLabel()));
						eqLabel.Union(this->tiles[x - 1][y].getLabel(), this->tiles[x][y - 1].getLabel());
					}
				}
				else if ((this->tiles[x - 1][y].getBiome() != this->tiles[x][y].getBiome()) && (this->tiles[x][y - 1].getBiome() == this->tiles[x][y].getBiome()))
				{
					this->tiles[x][y].setLabel(this->tiles[x][y - 1].getLabel());
				}
				else if ((this->tiles[x - 1][y].getBiome() != this->tiles[x][y].getBiome()) && (this->tiles[x][y - 1].getBiome() != this->tiles[x][y].getBiome()))
				{
					this->tiles[x][y].setLabel(eqLabel.get_nbSet());
					eqLabel.addSet();
				}
			}
			/*
			 * Conditions aux limites :
			 */

			else if (x == 0 && y != 0) //first column
			{
				if (this->tiles[x][y - 1].getBiome() == this->tiles[x][y].getBiome())
				{
					this->tiles[x][y].setLabel(this->tiles[x][y - 1].getLabel());
				}
				else
				{
					this->tiles[x][y].setLabel(eqLabel.get_nbSet());
					eqLabel.addSet();
				}
			}
			else if (y == 0 && x != 0) //first line
			{
				if (this->tiles[x - 1][y].getBiome() == this->tiles[x][y].getBiome())
				{
					this->tiles[x][y].setLabel(this->tiles[x - 1][y].getLabel());
				}
				else
				{
					this->tiles[x][y].setLabel(eqLabel.get_nbSet());
					eqLabel.addSet();
				}
			}
		}
	}

	for (int x = 0; x < mapSize.x; x++)
	{
		for (int y = 0; y < mapSize.y; y++)
		{
			this->tiles[x][y].setLabel(eqLabel.find(this->tiles[x][y].getLabel()));

			if (this->clusters.size() == 0)
			{
				this->tiles[x][y].setClusterID(0);
				this->clusters.push_back(std::vector<Tile*>());
				this->clusters[0].push_back(&this->tiles[x][y]);
			}
			else
			{
				int i = 0;
				while (i < (int)clusters.size() && this->clusters[i][0]->getLabel() != this->tiles[x][y].getLabel())
				{
					i++;
				}
				if (i == (int)clusters.size())
				{
					this->tiles[x][y].setClusterID(clusters.size());
					this->clusters.push_back(std::vector<Tile*>());
					this->clusters[i].push_back(&this->tiles[x][y]);
				}
				else
				{
					this->tiles[x][y].setClusterID(i);
					this->clusters[i].push_back(&this->tiles[x][y]);
				}
			}
		}
	}
}

void TileMap::cleanSmallCluster()
{
	for (int i = 0; i < (int)this->clusters.size(); i++)
	{
		if (this->clusters[i].size() < CLUSTER_MIN_SIZE)
		{
			int8_t dir = 0;
			if (this->clusters[i][0]->getInd().x < (int)(this->mapSize.x / 2.f))
				dir = 1;
			else
				dir = -1;

			sf::Vector2i position = this->clusters[i][0]->getInd();
			while (this->tiles[position.x][position.y].getClusterID() == this->clusters[i][0]->getClusterID())
			{
				position.x += dir; //on se dirrige vers le centre de la map
			}

			for (int j = 0; j < (int)clusters[i].size(); j++)
			{
				*this->clusters[i][j] = this->tiles[position.x][position.y];
				//this->clusters[i][j]->setDebug();
			}
		}
	}
}

/*************************************************************************
 * 			Accessors
 *************************************************************************/
sf::Vector2i TileMap::getMapSize() const
{
	return this->mapSize;
}

float TileMap::getMoveSpeed(sf::Vector2f pos) const
{
	sf::Vector2i indPos = vector2fToIndTile(pos);
	return this->tiles[indPos.x][indPos.y].getMoveSpeed();
}

ground_type TileMap::getGroundType(sf::Vector2f pos) const
{
	sf::Vector2i indPos = vector2fToIndTile(pos);
	return this->tiles[indPos.x][indPos.y].getGroundType();
}