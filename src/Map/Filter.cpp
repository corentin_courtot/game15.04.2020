#include "Filter.h"

Filter::Filter(uint8_t size, FilterType filterType) :
	size(size),
	filtertype(filterType)
{
	kernel = new float*[size];
	for (int i = 0; i < size; i++)
	{
		kernel[i] = new float[size];
	}

	switch (filtertype)
	{
		case BLUR:
			generateBlurKernel();
			break;
	}
}

Filter::~Filter()
{
	for (int i = 0; i < size; i++)
	{
		delete kernel[i];
	}
	delete kernel;
}

void Filter::generateBlurKernel()
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			kernel[i][j] = 1. / (size * size);
		}
	}
}