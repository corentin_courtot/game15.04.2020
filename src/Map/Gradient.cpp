#include "Gradient.h"

Gradient::Gradient(sf::Vector2i origin, int max, float coeff) :
	origin(origin),
	max(max),
	coeff(coeff)
{
	lMax = sqrt((300 - origin.x) * (300 - origin.x) + (200 - origin.y) * (200 - origin.y));
}

float Gradient::getGradient(int x, int y, sf::Vector2i mapSize)
{
	float projToOrigin = std::max(
		2.f * std::abs(x - origin.x) / mapSize.x,
		2.f * std::abs(y - origin.y) / mapSize.y); // vaut 0 au centre de la map, vaut 1 sur ses bords

	return std::max(-1.45f * projToOrigin * projToOrigin * projToOrigin * projToOrigin * projToOrigin * projToOrigin * projToOrigin * projToOrigin * projToOrigin * projToOrigin /* * projToOrigin * projToOrigin * projToOrigin*/,
		-projToOrigin * projToOrigin * projToOrigin * 10.f + 1.f); //entre 0 et -1
}