#include "Launcher.h"
#ifdef __linux__
	#include <X11/Xlib.h>
#endif

int main()
{
#ifdef __linux__
	XInitThreads();
#endif

	return Launcher(appMode::GAME_ONLY).run();
}