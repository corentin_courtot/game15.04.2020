#include "DataTransmitted.h"

void dataTransmittedToPacket(std::queue<DataTransmitted>& data, sf::Packet& packet)
{
	while (data.size() != 0)
	{
		packet << data.front();
		data.pop();
	}
}

void packetToDataTransmitted(sf::Packet& packet, std::queue<DataTransmitted>& data)
{
	while (!packet.endOfPacket())
	{
		DataTransmitted d;
		packet >> d;

		data.push(d);
	}
}

DataTransmitted::DataTransmitted(
	unsigned short objectType,
	unsigned short id,
	unsigned short belongId,
	unsigned short state,
	sf::Vector2f pos,
	sf::Vector2f target,
	sf::Vector2f velocity,
	sf::Vector2f scale,
	float angle,
	float damage,
	unsigned short health) :
	objectType(objectType),
	belongId(belongId),
	id(id),
	state(state),
	health(health),
	pos(pos),
	target(target),
	velocity(velocity),
	scale(scale),
	angle(angle),
	damage(damage)
{
}

sf::Packet& operator<<(sf::Packet& packet, const sf::Vector2f& v)
{
	return packet << v.x << v.y;
}

sf::Packet& operator>>(sf::Packet& packet, sf::Vector2f& v)
{
	return packet >> v.x >> v.y;
}

sf::Packet& operator<<(sf::Packet& packet, const DataTransmitted& DataTransmitted)
{
	return packet
		<< DataTransmitted.objectType
		<< DataTransmitted.id
		<< DataTransmitted.belongId
		<< DataTransmitted.state
		<< DataTransmitted.pos
		<< DataTransmitted.target
		<< DataTransmitted.velocity
		<< DataTransmitted.scale
		<< DataTransmitted.angle
		<< DataTransmitted.damage
		<< DataTransmitted.health
		<< DataTransmitted.equipment[0]
		<< DataTransmitted.equipment[1]
		<< DataTransmitted.equipment[2]
		<< DataTransmitted.body[0]
		<< DataTransmitted.body[1]
		<< DataTransmitted.body[2]
		<< DataTransmitted.body[3];
}

sf::Packet& operator>>(sf::Packet& packet, DataTransmitted& DataTransmitted)
{
	return packet
		>> DataTransmitted.objectType
		>> DataTransmitted.id
		>> DataTransmitted.belongId
		>> DataTransmitted.state
		>> DataTransmitted.pos
		>> DataTransmitted.target
		>> DataTransmitted.velocity
		>> DataTransmitted.scale
		>> DataTransmitted.angle
		>> DataTransmitted.damage
		>> DataTransmitted.health
		>> DataTransmitted.equipment[0]
		>> DataTransmitted.equipment[1]
		>> DataTransmitted.equipment[2]
		>> DataTransmitted.body[0]
		>> DataTransmitted.body[1]
		>> DataTransmitted.body[2]
		>> DataTransmitted.body[3];
}