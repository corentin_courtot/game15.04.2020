#pragma once

#include "DataTransmitted.h"
#include "ServerGame.h"

enum clientState
{
	INIT = 0,
	IN_GAME = 1
};

class Server
{
private:
	sf::TcpListener listener;
	std::vector<sf::TcpSocket*> clientsSockets;
	std::vector<clientState> clientsStates;

	std::queue<DataTransmitted> toClients;
	std::queue<DataTransmitted> fromClients;

	unsigned short seed;

	ServerGame serverGame;

	void initListening(const unsigned short& port);

	// Check if a client tried to connect
	void checkIncomingClients();
	// Envoie au client des donnees lui permettant d'init le jeu
	void sendInitData(int iClient);
	void messageExample();
	void receiveData();
	void handleIncomingData(const size_t& iClient, std::queue<DataTransmitted>& dataReceived);
	void sendGlobalData();

public:
	Server();
	~Server();

	void update();

	void run(const bool* endServer, const unsigned short& port);
};
