#pragma once
#include "Equipment.h"

class DataTransmitted
// structure used to transmit data between client and server about the entities
{
public:
	unsigned short objectType;					// ex : type of tree, main character, arrow,...
	unsigned short belongId;					// id of the team of the entity. Enables client to modify entity if the entity has the same team
	int id;										// unique id of the entity
	unsigned short state;						// state of the entity. ex : moving, aiming, resting, ...
	unsigned short health;						// health points of the entity
	sf::Vector2f pos;							// position of the entity
	sf::Vector2f target;						// target of the entity. An entity can either move or hit, hence 1 target is ok; offset for effects
	sf::Vector2f velocity;						// current speed of the entity (for projectiles)
	sf::Vector2f scale;							// scale of the entity's sprite
	float angle;								// angle of the entity
	float damage;								// damage dealt, used for effects
	unsigned short equipment[NB_EQUIPMENT - 1]; //equipement that the entity carries [weapon,torso, pants]; [id of ref entity, , ] for effects
	unsigned short body[4];						//body of the entity [hair, head, body, sex]

	DataTransmitted(
		unsigned short objectType = 0,
		unsigned short id = 0,
		unsigned short belongId = 0,
		unsigned short state = 0,
		sf::Vector2f pos = sf::Vector2f(0.f, 0.f),
		sf::Vector2f target = sf::Vector2f(0.f, 0.f),
		sf::Vector2f velocity = sf::Vector2f(0.f, 0.f),
		sf::Vector2f scale = sf::Vector2f(0.f, 0.f),
		float angle = 0.f,
		float damage = 0.f,
		unsigned short health = 0);
};

sf::Packet& operator<<(sf::Packet& packet, const sf::Vector2f& v);
sf::Packet& operator>>(sf::Packet& packet, sf::Vector2f& v);

sf::Packet& operator<<(sf::Packet& packet, const DataTransmitted& DataTransmitted);
sf::Packet& operator>>(sf::Packet& packet, DataTransmitted& DataTransmitted);

void dataTransmittedToPacket(std::queue<DataTransmitted>& data, sf::Packet& packet);
void packetToDataTransmitted(sf::Packet& packet, std::queue<DataTransmitted>& data);

void initData(DataTransmitted& data);
