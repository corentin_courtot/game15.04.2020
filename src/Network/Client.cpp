#include "Client.h"

bool Client::tryToConnect(const unsigned short& port)
{
	std::cout << "(Client) Trying to connect to server " << std::endl;
	sf::sleep(sf::seconds(2));

	// Connect to the server
	if (this->socket.connect(sf::IpAddress::getLocalAddress(), port) != sf::Socket::Done)
		return false;
	std::cout << "(Client) Connected to server " << this->socket.getRemoteAddress() << std::endl;

	this->socket.setBlocking(false);

	return true;
}

void Client::messageExample()
{
	//std::cout << "sending message to server " << std::endl;
	// Receive a message from the server
	char in[128];
	std::size_t received;
	if (this->socket.receive(in, sizeof(in), received) != sf::Socket::Done)
		return;
	//std::cout << "Message received from the server: \"" << in << "\"" << std::endl;

	// Send an answer to the server
	const char out[] = "Hi, I'm a client";
	if (this->socket.send(out, sizeof(out)) != sf::Socket::Done)
		return;
	//std::cout << "Message sent to the server: \"" << out << "\"" << std::endl;
}

void Client::sendData(std::queue<DataTransmitted>& toServer)
{
	if (toServer.size() == 0)
	{
		return;
	}

	// std::cout << "(Client) Data transmitted to server : "
	// 		  << "objectType : " << toServer.front().objectType
	// 		  << " id : " << toServer.front().id
	// 		  << " belongid : " << toServer.front().belongId
	// 		  << " scale:" << toServer.front().scale
	// 		  << " health:" << toServer.front().health
	// 		  << " pos:" << toServer.front().pos.x << "," << toServer.front().pos.y
	// 		  << " target:" << toServer.front().target.x << "," << toServer.front().target.y
	// 		  << " state:" << toServer.front().state
	// 		  << std::endl;

	sf::Packet packet;
	// traduction des données en sf::Packet
	dataTransmittedToPacket(toServer, packet);

	this->socket.send(packet);
}

void Client::receiveData(std::queue<DataTransmitted>& fromServer, std::mutex& fromServerLock)
{
	sf::Packet packet;

	if (this->socket.receive(packet) != sf::Socket::Done)
	{
		// si aucune donnee n'a ete recue
		return;
	}

	fromServerLock.lock();

	do
	{
		// on ecrase les donnes precedemment recues
		fromServer = std::queue<DataTransmitted>();

		// traduction des données entrantes
		packetToDataTransmitted(packet, fromServer);
	} while (this->socket.receive(packet) == sf::Socket::Done);

	fromServerLock.unlock();
}

Client::Client() :
	state(LOOKING_FOR_CONNECTION)
{
	this->socket.setBlocking(true);
}

Client::~Client()
{
}

void Client::run(const bool* endClient,
	const unsigned short& port,
	std::queue<DataTransmitted>& toServer,
	std::queue<DataTransmitted>& fromServer,
	std::mutex& fromServerLock,
	bool& isConnected)
{
	while (!*endClient)
	{
		if (!isConnected)
		{
			isConnected = this->tryToConnect(port);
		}
		else
		{
			this->sendData(toServer);
			this->receiveData(fromServer, fromServerLock);
			//this->messageExample();
			sf::sleep(sf::milliseconds(10));
		}
		sf::sleep(sf::milliseconds(30));
	}

	std::cout << "(Client) closing" << std::endl;
}
