#pragma once

#include "Character.h"
#include "DataTransmitted.h"
#include "Effect.h"
#include "Entity.h"
#include "Fauna.h"
#include "GameMap.h"
#include "PauseMenu.h"
#include "Projectile.h"
#include "SelectedEntity.h"
#include "State.h"

class ServerGame
{
private:
	// Elements on the map
	std::vector<Entity*> mapEntities;

	// All entities sent to clients
	std::vector<Entity*> entities;

	// time
	sf::Clock dtClock;
	float dt;

	// Map
	GameMap* gameMap;

public:
	ServerGame();
	~ServerGame();

	void initGame(unsigned short seed);
	void initNewPlayer(int idPlayer, int nbPlayer);

	// update toClients according to current entities
	void updateToClients(std::queue<DataTransmitted>& toClients);
	// handles any data incoming from client
	void handleClientData(std::queue<DataTransmitted>& fromClients, const int idPlayer);

	// fais tourner le jeu
	void update(std::queue<DataTransmitted>& fromClients);
};
