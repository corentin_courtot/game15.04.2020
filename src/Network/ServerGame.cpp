#include "ServerGame.h"

ServerGame::ServerGame()
{
	this->entities.resize(0);
}

ServerGame::~ServerGame()
{
}

void ServerGame::initGame(unsigned short seed)
{
	this->gameMap = new GameMap(tileSize, mapSize.x, mapSize.y, seed, this->entities, this->mapEntities);

	this->entities.push_back(new Character(sf::Vector2f(3050.f, 2500.f), head1, bodyF1, hairF1, FEMALE, freeId(this->entities), 10));
	this->entities.push_back(new Character(sf::Vector2f(3000.f, 2500.f), head1, bodyM1, hairM1, MALE, freeId(this->entities), 11));
	this->entities.push_back(new Character(sf::Vector2f(2950.f, 2500.f), head1, bodyM1, hairM1, MALE, freeId(this->entities), 12));
	this->entities.push_back(new Rabbit(sf::Vector2f(2900.f, 2500.f), freeId(this->entities), 0));

	//this->entities[0]->setMovementTarget(sf::Vector2f(2900.f, 5000.f));
	//this->entities[1]->setMovementTarget(sf::Vector2f(2000.f, 3500.f));
	//this->entities[2]->setMovementTarget(sf::Vector2f(1500.f, 2500.f));
}

void ServerGame::initNewPlayer(int idPlayer, int nbPlayer)
{
	this->entities.push_back(new Character(sf::Vector2f(2050.f, 1500.f), head1, bodyF1, hairF1, FEMALE, freeId(this->entities), 10));
}

void ServerGame::updateToClients(std::queue<DataTransmitted>& toClients)
{
	deleteHolesAndSort(this->entities);

	for (size_t i = 0; i < this->entities.size(); i++)
	{
		DataTransmitted data = DataTransmitted();

		if (this->entities[i])
			this->entities[i]->toDataTransmitted(data);

		toClients.push(data);
	}
}

void ServerGame::handleClientData(std::queue<DataTransmitted>& fromClients, const int idPlayer)
{
	while (fromClients.size() > 0)
	{
		if (fromClients.front().belongId == idPlayer + 10) // si le client a bien le droit de modifier l'entite
		{
			int entityIndex = getIndex(this->entities, fromClients.front().id);
			if (entityIndex != -1)
				this->entities[entityIndex]->updateAccordingToClientData(fromClients.front());
			else
			{
				std::cout << "(ServerGame) This entity sent by " << idPlayer + 10
						  << "is unknown. objectType:" << fromClients.front().objectType
						  << " id:" << fromClients.front().id
						  << " scale:" << fromClients.front().scale.x
						  << " health:" << fromClients.front().health
						  << " pos:" << fromClients.front().pos.x << "," << fromClients.front().pos.y
						  << " target:" << fromClients.front().target.x << "," << fromClients.front().target.y
						  << " state:" << fromClients.front().state << std::endl;
			}
		}
		else
		{
			std::cout << "(ServerGame) [error] not allowed to modify this entity. entity.belongId : "
					  << fromClients.front().belongId
					  << " playerId : " << idPlayer + 10
					  << std::endl;
		}
		fromClients.pop();
	}
}

void ServerGame::update(std::queue<DataTransmitted>& fromClients)
{
	sf::sleep(sf::milliseconds(positif(10.f - this->dtClock.getElapsedTime().asMilliseconds())));

	//Updates the dt variable with the time it takes to update and render one frame
	this->dt = this->dtClock.restart().asSeconds();

	// debug
	//std::cout << "(ServerGame) entities display : " << std::endl;
	//displayEntities(this->entities);

	deleteHolesAndSort(this->entities);

	for (size_t i = 0; i < this->entities.size(); i++)
	{
		if (this->entities[i])
		{
			if (!this->entities[i]->update(this->dt, this->entities, this->mapEntities, this->gameMap))
			{
				delete this->entities[i];
				this->entities[i] = NULL;
			}
		}
	}

	for (size_t i = 0; i < this->mapEntities.size(); i++)
	{
		if (this->mapEntities[i])
		{
			if (!this->mapEntities[i]->update(this->dt, this->entities, this->mapEntities, this->gameMap))
			{
				delete this->mapEntities[i];
				this->mapEntities[i] = NULL;
			}
		}
	}
}