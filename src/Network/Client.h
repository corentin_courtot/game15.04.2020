#pragma once

#include "DataTransmitted.h"

enum ClientState
{
	LOOKING_FOR_CONNECTION = 0,
	CONNECTED
};

class Client
{
private:
	ClientState state;

	sf::TcpSocket socket;

	bool tryToConnect(const unsigned short& port);
	void messageExample();
	// send toServer to the server
	void sendData(std::queue<DataTransmitted>& toServer);
	// receive data from server
	void receiveData(std::queue<DataTransmitted>& fromServer, std::mutex& fromServerLock);

public:
	Client();
	~Client();

	void run(
		const bool* endClient,
		const unsigned short& port,
		std::queue<DataTransmitted>& toServer,
		std::queue<DataTransmitted>& fromServer,
		std::mutex& fromServerLock,
		bool& isConnected);
};