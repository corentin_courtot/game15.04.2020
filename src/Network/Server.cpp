#include "Server.h"

void Server::initListening(const unsigned short& port)
{
	// Listen to the given port for incoming connections
	if (this->listener.listen(port) != sf::Socket::Done)
	{
		std::cout << "(Server) Failed to init listening " << std::endl;
		return;
	}
	std::cout << "(Server) Listening to port " << port << ", waiting for connections... " << std::endl;

	std::cout << "(Server) IP adress of the server : " << sf::IpAddress::getLocalAddress() << std::endl;

	this->listener.setBlocking(false);
}

void Server::checkIncomingClients()
{
	this->clientsSockets.push_back(new sf::TcpSocket);
	this->clientsStates.push_back(clientState::INIT);
	if (this->listener.accept(*clientsSockets[clientsSockets.size() - 1]) != sf::Socket::Done)
	{
		delete this->clientsSockets[clientsSockets.size() - 1];
		this->clientsSockets.pop_back();
		this->clientsStates.pop_back();
		return;
	}
	std::cout << "(Server) Client connected: " << clientsSockets[clientsSockets.size() - 1]->getRemoteAddress() << std::endl;

	clientsSockets[clientsSockets.size() - 1]->setBlocking(false);
}

void Server::sendInitData(int iClient)
{
	std::queue<DataTransmitted> data;
	data.push(DataTransmitted(this->seed, iClient + 10));
	sf::Packet packet;

	//std::cout << "(Server) Seed " << data.front().objectType << " sent to client with id " << data.front().id << std::endl;

	dataTransmittedToPacket(data, packet);
	clientsSockets[iClient]->send(packet);
}

void Server::messageExample()
{
	for (size_t i = 0; i < clientsSockets.size(); i++)
	{
		// Send a message to the connected client
		const char out[] = "Hi, I'm the server";
		if (clientsSockets[i]->send(out, sizeof(out)) != sf::Socket::Done)
			return;
		std::cout << "Message sent to the client: \"" << out << "\"" << std::endl;

		// Receive a message back from the client
		char in[128];
		std::size_t received;
		if (clientsSockets[i]->receive(in, sizeof(in), received) != sf::Socket::Done)
			return;
		std::cout << "Answer received from the client: \"" << in << "\"" << std::endl;
	}
}

void Server::sendGlobalData()
{
	sf::Packet globalPacket;

	// traduction des données en sf::Packet
	dataTransmittedToPacket(this->toClients, globalPacket);

	for (size_t i = 0; i < clientsSockets.size(); i++)
	{
		switch (this->clientsStates[i])
		{
			case INIT:
				this->sendInitData(i);
				break;

			case IN_GAME:
				this->clientsSockets[i]->send(globalPacket);
				break;

			default:
				break;
		}
	}
}

void Server::receiveData()
{
	//std::cout << "(Server) start receiveData()" << std::endl;

	for (size_t i = 0; i < clientsSockets.size(); i++)
	{
		sf::Packet packet;

		if (this->clientsSockets[i]->receive(packet) == sf::Socket::Done)
		{
			packetToDataTransmitted(packet, this->fromClients);

			this->handleIncomingData(i, this->fromClients);
		}
	}
}

void Server::handleIncomingData(const size_t& iClient, std::queue<DataTransmitted>& dataReceived)
{
	if (iClient > this->clientsStates.size())
	{
		std::cout << "(Server) error iClient in handleIncomingData" << std::endl;
	}

	switch (this->clientsStates[iClient])
	{
		case INIT:
			if (dataReceived.front().objectType == 0 && dataReceived.front().id == 1)
			{
				this->clientsStates[iClient] = IN_GAME;
				std::cout << "(Server) Client entered game" << std::endl;
			}
			while (!dataReceived.empty())
			{
				dataReceived.pop();
			}
			break;

		case IN_GAME:
			if (dataReceived.front().objectType == 0 && dataReceived.front().id == 2)
			{
				this->clientsStates[iClient] = INIT;
				std::cout << "(Server) Client quit game" << std::endl;
			}
			else
			{
				// std::cout << "(Server) Data received from client : " << std::endl;

				// while (dataReceived.size() > 0 && dataReceived.front().objectType != 0)
				// {
				// 	std::cout << "objectType : " << dataReceived.front().objectType
				// 			  << " id : " << dataReceived.front().id
				// 			  << " belongid : " << dataReceived.front().belongId
				// 			  << " scale:" << dataReceived.front().scale
				// 			  << " health:" << dataReceived.front().health
				// 			  << " pos:" << dataReceived.front().pos.x << "," << dataReceived.front().pos.y
				// 			  << " target:" << dataReceived.front().target.x << "," << dataReceived.front().target.y
				// 			  << " state:" << dataReceived.front().state
				// 			  << std::endl;
				// 	dataReceived.pop();
				// }
				this->serverGame.handleClientData(dataReceived, iClient);
			}
			break;

		default:
			break;
	}
}

Server::Server()
{
	myRand::srand(time(NULL));
	//myRand::rand(); // used to increase rand() dispersion
	this->seed = (unsigned short)((myRand::rand() / (float)myRand::randMax) * 65535.f);

	this->serverGame.initGame(this->seed);
}

Server::~Server()
{
	while (clientsSockets.size() != 0)
	{
		delete this->clientsSockets[clientsSockets.size() - 1];
		this->clientsSockets.pop_back();
	}
}

void Server::update()
{
	//std::cout << "(Server) updating" << std::endl;

	this->checkIncomingClients();
	this->receiveData();

	this->serverGame.update(this->fromClients);
	this->serverGame.updateToClients(this->toClients);

	this->sendGlobalData();
}

void Server::run(const bool* endServer, const unsigned short& port)
{
	std::cout << "(Server) running. seed used : " << this->seed << std::endl;

	this->initListening(port);

	while (!*endServer)
	{
		this->update();

		sf::sleep(sf::milliseconds(0));
	}

	std::cout << "(Server) closing" << std::endl;
}