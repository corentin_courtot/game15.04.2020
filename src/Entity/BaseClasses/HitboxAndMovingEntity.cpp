#include "HitboxAndMovingEntity.h"

HitboxAndMovingEntity::HitboxAndMovingEntity(EntityType entityType, int id, unsigned short belongId) :
	Entity(entityType, id, belongId),
	MovingEntity(entityType, id, belongId),
	HitboxEntity(entityType, id, belongId),
	hasCollided(false)
{
}

HitboxAndMovingEntity::~HitboxAndMovingEntity()
{
}

bool HitboxAndMovingEntity::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	this->hasCollided = false;

	this->movementComponent->update(dt * gameMap->getMoveSpeed(this->sprite.getPosition())); // Fais bouger l'entite selon sa vitesse actuelle

	this->hitboxComponent->update(); // Replace la Hitbox a l'endroit ou se trouve effectivement le l'entite

	if (this->getEntitiesColliding(entities, mapEntities).size())
	// annule le mouvement si celui-ci fait rentrer en collision avec une autre entite
	{
		this->hasCollided = true;

		this->movementComponent->update(-dt * gameMap->getMoveSpeed(this->sprite.getPosition()));

		this->hitboxComponent->update();

		this->setMovementTarget(this->getCoord());
	}

	return Entity::update(dt, entities, mapEntities, gameMap, visibilityMap);
}
