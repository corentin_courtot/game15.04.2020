#include "HitboxEntity.h"

HitboxEntity::HitboxEntity(EntityType entityType, int id, unsigned short belongId) :
	Entity(entityType, id, belongId)
{
	this->hitboxComponent = NULL;
}

HitboxEntity::~HitboxEntity()
{
	if (this->hitboxComponent)
	{
		delete this->hitboxComponent;
		this->hitboxComponent = NULL;
	}
}

HitboxComponent* HitboxEntity::getHitboxComponent() const
{
	return this->hitboxComponent;
}

void HitboxEntity::setPosition(const sf::Vector2f& pos)
{
	Entity::setPosition(pos);

	if (this->hitboxComponent)
		this->hitboxComponent->update(); // Replace la Hitbox a l'endroit ou se trouve effectivement le l'entite
}

void HitboxEntity::createHitboxComponent(sf::Sprite& sprite,
	float offset_x, float offset_y,
	float width, float height, float angle)
{
	this->hitboxComponent = new HitboxComponent(sprite, offset_x, offset_y, width, height, angle);
}

std::vector<Entity*> HitboxEntity::getEntitiesColliding(std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities) const
{
	std::vector<Entity*> returnVector;
	// traitement des entites
	for (size_t i = 0; i < entities.size(); ++i)
	{
		if (entities[i]
			&& entities[i] != this
			&& this->hitboxComponent->checkIntersect(entities[i]->getHitboxComponent()))
		{
			returnVector.push_back(entities[i]);
		}
	}
	// traitement des mapEntites
	for (size_t i = 0; i < mapEntities.size(); ++i)
	{
		if (mapEntities[i]
			&& mapEntities[i] != this
			&& this->hitboxComponent->checkIntersect(mapEntities[i]->getHitboxComponent()))
		{
			returnVector.push_back(mapEntities[i]);
		}
	}

	return returnVector;
}

bool HitboxEntity::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	return Entity::update(dt, entities, mapEntities, gameMap, visibilityMap);
}