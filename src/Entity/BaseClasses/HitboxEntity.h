#pragma once

#include "Entity.h"
#include "HitboxComponent.h"

class HitboxEntity : public virtual Entity
{
private:
protected:
	HitboxComponent* hitboxComponent;

public:
	HitboxEntity(EntityType entityType, int id = 0, unsigned short belongId = 0);
	~HitboxEntity();

	virtual HitboxComponent* getHitboxComponent() const;
	virtual void setPosition(const sf::Vector2f& pos);

	void createHitboxComponent(sf::Sprite& sprite,
		float offset_x, float offset_y,
		float width, float height, float angle = 0.f);

	// renvoie l'ensemble des entites entrees en collision avec l'entite actuelle
	virtual std::vector<Entity*> getEntitiesColliding(std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities) const;

	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
};