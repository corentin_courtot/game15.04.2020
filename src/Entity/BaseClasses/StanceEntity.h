#pragma once

#include "Entity.h"
#include "StanceComponent.h"

class StanceEntity : public virtual Entity
{
private:
protected:
	StanceComponent* stanceComponent;

public:
	StanceEntity(EntityType entityType, int id = 0, unsigned short belongId = 0);
	~StanceEntity();
};
