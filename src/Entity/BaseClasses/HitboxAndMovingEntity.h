#pragma once

#include "GameMap.h"
#include "HitboxEntity.h"
#include "MovingEntity.h"

class HitboxAndMovingEntity : public MovingEntity, public HitboxEntity
{
private:
protected:
	bool hasCollided;

public:
	HitboxAndMovingEntity(EntityType entityType, int id = 0, unsigned short belongId = 0);
	~HitboxAndMovingEntity();

	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
};