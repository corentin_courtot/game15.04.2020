#pragma once

#include "Entity.h"
#include "MovementComponent.h"

class MovingEntity : public virtual Entity
// Une movingEntity est une entite dont le deplacement est complexe : sa vitesse peut varier au cours du temps
{
private:
protected:
	MovementComponent* movementComponent;

	bool isOnMap;

	void initVariables();

public:
	MovingEntity(EntityType entityType, int id = 0, unsigned short belongId = 0);
	~MovingEntity();

	//Component functions
	void createMovementComponent(const float maxVelocity);

	//Functions
	virtual void setMovementTarget(const sf::Vector2f& target);

	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
	virtual void render(sf::RenderTarget& target) const;
};
