#pragma once

#include "BodyPart.h"
#include "DamageStatistic.h"
#include "DataTransmitted.h"
#include "EntityData.h"
#include "EntityEnum.h"
#include "GeneralFunctions.h"
#include "HitboxComponent.h"
#include "VisibilityMap.h"

class GameMap; // Forward declaration of GameMap - see https://stackoverflow.com/questions/4757565/what-are-forward-declarations-in-c for more info

class Entity
{
private:
protected:
	sf::Sprite sprite;
	sf::Vector2f textureSize;

	bool isMovingEntity;
	bool renderingPriority;

	EntityType entityType;
	int id;
	unsigned short belongId;

	void initVariables();

public:
	bool isInViewRange;

	Entity(EntityType entityType, int id = 0, unsigned short belongId = 0);
	virtual ~Entity();

	// Accessors
	virtual HitboxComponent* getHitboxComponent() const;
	const sf::Vector2f getCoord() const;
	const sf::Vector2f getOrigin() const;
	const sf::Vector2f getTextureSize() const;
	const bool canMove() const;
	bool getRenderingPriority() const;
	const int getId() const;
	const int getBelongId() const;

	void setTexture(const sf::Texture& texture);

	//
	virtual void setPosition(const sf::Vector2f& pos);
	virtual void setMovementTarget(const sf::Vector2f& target); // ne s'applique que pour les movingEntity
	virtual void setWeaponTarget(const sf::Vector2f& target);	// ne s'applique que pour les character

	// Functions
	virtual void getHit(std::vector<Entity*>& entities, DamageStatistic& damageStatistic);
	virtual bool isClicked(sf::Vector2f mouse_pos);

	//* Network
	// upload all entity info in data
	virtual void toDataTransmitted(DataTransmitted& data) const;
	// update the entity according to data coming from the server
	virtual void updateAccordingToServerData(DataTransmitted& data);
	// update the entity according to data coming from a client
	virtual void updateAccordingToClientData(DataTransmitted& data);

	// Main functions
	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
	virtual void render(sf::RenderTarget& target) const;

	// Fonctions amies
	friend bool compareEntitiesId(Entity* entity1, Entity* entity2);
	friend void displayEntities(const std::vector<Entity*>& entities);
};

void deleteHolesAndSort(std::vector<Entity*>& entities);

int freeId(const std::vector<Entity*>& entities);

int getIndex(const std::vector<Entity*>& entities, const int id);
