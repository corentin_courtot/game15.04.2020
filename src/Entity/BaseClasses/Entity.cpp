
#include "Entity.h"

void Entity::initVariables()
{
	this->textureSize = sf::Vector2f(0, 0);

	this->isMovingEntity = false;

	this->renderingPriority = false;

	this->isInViewRange = false;
}

Entity::Entity(EntityType entityType, int id, unsigned short belongId) :
	entityType(entityType),
	id(id),
	belongId(belongId)
{
	this->initVariables();
}

Entity::~Entity()
{
}

HitboxComponent* Entity::getHitboxComponent() const
{
	return NULL;
}

const sf::Vector2f Entity::getCoord() const
{
	return this->sprite.getPosition();
}

const sf::Vector2f Entity::getOrigin() const
{
	return this->sprite.getOrigin();
}

const sf::Vector2f Entity::getTextureSize() const
{
	return this->textureSize;
}

const bool Entity::canMove() const
{
	return this->isMovingEntity;
}
bool Entity::getRenderingPriority() const
{
	return this->renderingPriority;
}

const int Entity::getId() const
{
	return this->id;
}

const int Entity::getBelongId() const
{
	return this->belongId;
}

void Entity::setTexture(const sf::Texture& texture)
{
	this->sprite.setTexture(texture);
}

void Entity::setPosition(const sf::Vector2f& pos)
{
	this->sprite.setPosition(pos);
}

void Entity::setMovementTarget(const sf::Vector2f& target)
{
	std::cout << "Erreur : setMovementTarget appelé pour une Entite sans movementComponent" << std::endl;
}

void Entity::setWeaponTarget(const sf::Vector2f& target)
{
	std::cout << "Erreur : setWeaponTarget appelé pour une Entite ne pouvant pas se battre" << std::endl;
}

bool Entity::isClicked(sf::Vector2f mouse_pos)
{
	return this->sprite.getGlobalBounds().contains(mouse_pos);
}

void Entity::getHit(std::vector<Entity*>& entities, DamageStatistic& damageStatistic)
{
	return;
}

void Entity::toDataTransmitted(DataTransmitted& data) const
{
	data.objectType = this->entityType;
	data.id = this->id;
	data.belongId = this->belongId;
	data.pos = this->sprite.getPosition();
	data.scale = this->sprite.getScale();
	data.angle = this->sprite.getRotation();
}

void Entity::updateAccordingToServerData(DataTransmitted& data)
{
	this->belongId = data.belongId;
	this->id = data.id;
	if (squareDist(data.pos - this->getCoord()) > 8)
		this->setPosition(data.pos);
	if (abs(this->sprite.getRotation() - data.angle) > 5)
		this->sprite.setRotation(data.angle);
	if (this->sprite.getScale() != data.scale)
		this->sprite.setScale(data.scale);
}

void Entity::updateAccordingToClientData(DataTransmitted& data)
{
}

bool Entity::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	return true;
}

void Entity::render(sf::RenderTarget& target) const
{
	if (this->isInViewRange)
		target.draw(this->sprite);
}

bool compareEntitiesId(Entity* entity1, Entity* entity2)
{
	return entity1->id < entity2->id;
}

void displayEntities(const std::vector<Entity*>& entities)
{
	for (size_t i = 0; i < entities.size(); i++)
	{
		if (entities[i])
		{
			std::cout << "Entity :"
					  << " id : " << entities[i]->id
					  << " entityType : " << entities[i]->entityType
					  << " pos : " << entities[i]->getCoord().x << "," << entities[i]->getCoord().y
					  << " belongId : " << entities[i]->belongId
					  << std::endl;
		}
	}
}

void deleteHolesAndSort(std::vector<Entity*>& entities)
{
	// first : deleting holes in the vect
	size_t i = 0;
	while (i < entities.size())
	{
		while (i < entities.size() && entities[i] == NULL) // while ith entity is a hole
		{
			entities[i] = entities[entities.size() - 1];
			entities.pop_back();
			// Note : no delete is called because we don't want the entity to disappear
			// but to move its adress location in memory
		}
		++i;
	}

	// second : sort entities by id ascending order
	std::sort(entities.begin(), entities.end(), compareEntitiesId);
}

int freeId(const std::vector<Entity*>& entities)
{
	int i = 0;
	while (i < (int)entities.size() && entities[i] && entities[i]->getId() == i + 1)
	{
		++i;
	}
	return i + 1;
}

int getIndex(const std::vector<Entity*>& entities, const int id)
{
	int i = 0;
	while (i < (int)entities.size() && entities[i] && entities[i]->getId() <= id)
	{
		if (entities[i]->getId() == id)
		{
			return i;
		}
		++i;
	}
	return -1;
}