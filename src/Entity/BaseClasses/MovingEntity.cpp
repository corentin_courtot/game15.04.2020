#include "MovingEntity.h"

void MovingEntity::initVariables()
{
	Entity::initVariables();

	this->movementComponent = NULL;

	this->isOnMap = true;

	this->isMovingEntity = true;
}

MovingEntity::MovingEntity(EntityType entityType, int id, unsigned short belongId) :
	Entity(entityType, id, belongId)
{
	this->MovingEntity::initVariables();
}

MovingEntity::~MovingEntity()
{
	delete this->movementComponent;
}

void MovingEntity::createMovementComponent(const float maxVelocity)
{
	this->movementComponent = new MovementComponent(this->sprite, maxVelocity);
}

//Functions
void MovingEntity::setMovementTarget(const sf::Vector2f& target)
{
	this->movementComponent->setMovementTarget(IndTileToVector2f(vector2fToIndTile(target)));
}

bool MovingEntity::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	this->movementComponent->update(dt); // Fais bouger l'entite selon sa vitesse actuelle

	return Entity::update(dt, entities, mapEntities, gameMap, visibilityMap);
}

void MovingEntity::render(sf::RenderTarget& target) const
{
	Entity::render(target);
}
