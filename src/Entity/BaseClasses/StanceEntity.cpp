#include "StanceEntity.h"

StanceEntity::StanceEntity(EntityType entityType, int id, unsigned short belongId) :
	Entity(entityType, id, belongId)
{
	this->stanceComponent = NULL;
}

StanceEntity::~StanceEntity()
{
	if (this->stanceComponent)
	{
		delete this->stanceComponent;
		this->stanceComponent = NULL;
	}
}