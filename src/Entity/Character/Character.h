#pragma once

#include "BodyPart.h"
#include "CharacterStanceComponent.h"
#include "Effect.h"
#include "EquipmentComponent.h"
#include "EquipmentData.h"
#include "HitboxAndMovingEntity.h"
#include "HitboxEffect.h"
#include "Projectile.h"
#include "StatisticComponent.h"

enum sex_
{
	MALE = 0,
	FEMALE
};

class Character :
	public HitboxAndMovingEntity
{
private:
	//Variables
	Head* head;
	Body* body;
	Hair* hair;

	EquipmentComponent* equipment;
	CharacterStanceComponent* stanceComponent;
	StatisticComponent* statisticComponent;

	sex_ sex;

	sf::RectangleShape* waterRectangle;

	int viewRange;
	bool isAlliedToPlayer;

	//* Initializer functions
	void initVariables(HeadType head, BodyType body, HairType hair, sex_ sex);
	void initComponents();
	void createStanceComponent(Head* head, Body* body, Hair* hair = NULL);
	void createEquipmentComponent();
	void createStatisticComponent();

	//* update functions
	virtual void updateStance();
	virtual void updateWaterRect(GameMap* gameMap);
	virtual void updateFieldOfView(std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities);

public:
	Character(sf::Vector2f pos,
		HeadType head = head1,
		BodyType body = bodyM1,
		HairType hair = hairM1,
		sex_ sex = MALE,
		int id = 0,
		unsigned short belongId = 1,
		bool isAlliedToPlayer = true);
	Character(const DataTransmitted& data, bool isAlliedToPlayer);
	~Character();

	//* Accessors

	//Functions
	virtual void getHit(std::vector<Entity*>& entities, DamageStatistic& damageStatistic);
	virtual void setMovementTarget(const sf::Vector2f& target);
	virtual void setWeaponTarget(const sf::Vector2f& target);
	virtual void addEquipment(EquipmentName equipmentName);
	virtual void addWeapon(unsigned short weaponName);

	//* Network
	virtual void toDataTransmitted(DataTransmitted& data) const;
	// only the client will enter this function to update the character according to server
	virtual void updateAccordingToServerData(DataTransmitted& data);
	// only the server will enter this function to update the character according to client
	virtual void updateAccordingToClientData(DataTransmitted& data);

	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
	virtual void render(sf::RenderTarget& target) const;
};
