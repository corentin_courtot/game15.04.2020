
#include "Character.h"

//Initializer Functions
void Character::initVariables(HeadType head, BodyType body, HairType hair, sex_ sex)
{
	MovingEntity::initVariables();

	this->head = new Head(head);
	this->body = new Body(body);
	this->hair = new Hair(hair);

	this->sex = sex;

	this->viewRange = 900;
}

void Character::initComponents()
{
	this->createHitboxComponent(this->sprite, 0.f, -13.f, 18.f, 45.f);
	this->createMovementComponent(200.f);
	this->createStanceComponent(this->head, this->body, this->hair);

	this->equipment = new EquipmentComponent();
	this->createEquipmentComponent();
	this->createStatisticComponent();
}

void Character::createStanceComponent(Head* head, Body* body, Hair* hair)
{
	this->stanceComponent = new CharacterStanceComponent(this->sprite);
	this->stanceComponent->setupCharacterStance(head, body, hair);
}

void Character::createEquipmentComponent()
{
	if (this->sex == MALE)
	{
		this->addEquipment(tShirtM1);
		this->addEquipment(pant1);
		this->addWeapon(bow);
	}
	else
	{
		this->addEquipment(tShirtF1);
		this->addEquipment(pant2);
		this->addWeapon(woodenSword);
	}
}

void Character::createStatisticComponent()
{
	this->statisticComponent = new StatisticComponent(this->sprite);
}

//* update functions
void Character::updateStance()
{
	if (this->movementComponent == NULL || this->equipment == NULL)
	{
		return;
	}

	std::string stance = "front";
	switch (this->movementComponent->getState())
	{
		case (IDLE):
			break;

		case (WALKING_DOWN):
			break;

		case (WALKING_LEFT):
			stance = "facingLeft";
			break;

		case (WALKING_RIGHT):
			stance = "facingRight";
			break;

		case (WALKING_UP):
			stance = "back";
			break;

		default:
			break;
	}

	if (this->equipment->getWeapon() && !this->equipment->getWeapon()->getTarget())
		stance += "Weapon";

	this->stanceComponent->play(stance);
}

void Character::updateWaterRect(GameMap* gameMap)
{
	if (gameMap->getGroundType(this->sprite.getPosition()) == ground_type::WATER)
	{
		if (!this->waterRectangle)
		{
			this->waterRectangle = new sf::RectangleShape();
			this->waterRectangle->setSize(sf::Vector2f(30.f, 33.f));
			this->waterRectangle->setOrigin(sf::Vector2f(15.f, 20.f));
			this->waterRectangle->setFillColor(sf::Color(14, 90, 152, 180));
		}
		this->waterRectangle->setPosition(this->sprite.getPosition());
	}
	else if (this->waterRectangle && gameMap->getGroundType(this->sprite.getPosition()) != ground_type::WATER)
	{
		delete this->waterRectangle;
		this->waterRectangle = NULL;
	}
}

void Character::updateFieldOfView(std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities)
{
	this->isInViewRange = true;

	for (size_t i = 0; i < entities.size(); i++)
	{
		if (entities[i])
		{
			if (distSquared(this->sprite.getPosition() - entities[i]->getCoord()) < this->viewRange * this->viewRange)
			{
				entities[i]->isInViewRange = true;
			}
		}
	}

	for (size_t i = 0; i < mapEntities.size(); i++)
	{
		if (mapEntities[i])
		{
			if (distSquared(this->sprite.getPosition() - mapEntities[i]->getCoord()) < this->viewRange * this->viewRange)
			{
				mapEntities[i]->isInViewRange = true;
			}
		}
	}
}

//* Constructor & Destructors
Character::Character(sf::Vector2f pos,
	HeadType head,
	BodyType body,
	HairType hair,
	sex_ sex,
	int id,
	unsigned short belongId,
	bool isAlliedToPlayer) :
	Entity(player, id, belongId),
	HitboxAndMovingEntity(player, id, belongId),
	isAlliedToPlayer(isAlliedToPlayer)
{
	this->initVariables(head, body, hair, sex);

	this->setPosition(pos);

	this->initComponents();

	this->textureSize = sf::Vector2f(30.f, 60.f);
	this->sprite.setOrigin(sf::Vector2f(15.f, 45.f));

	this->waterRectangle = NULL;
}

Character::Character(const DataTransmitted& data, bool isAlliedToPlayer) :
	Entity((EntityType)data.objectType, data.id, data.belongId),
	HitboxAndMovingEntity((EntityType)data.objectType, data.id, data.belongId),
	isAlliedToPlayer(isAlliedToPlayer)
{
	this->initVariables((HeadType)data.body[1], (BodyType)data.body[2], (HairType)data.body[0], (sex_)data.body[3]);

	this->setPosition(data.pos);

	this->initComponents();

	if (data.equipment[0])
		this->addWeapon(data.equipment[0]);

	if (data.equipment[1])
		this->addEquipment((EquipmentName)data.equipment[1]);

	if (data.equipment[2])
		this->addEquipment((EquipmentName)data.equipment[2]);

	this->textureSize = sf::Vector2f(30.f, 60.f);
	this->sprite.setOrigin(sf::Vector2f(15.f, 45.f));

	this->waterRectangle = NULL;
}

Character::~Character()
{
	if (this->head)
	{
		delete this->head;
		this->head = NULL;
	}
	if (this->body)
	{
		delete this->body;
		this->body = NULL;
	}
	if (this->equipment)
	{
		delete this->equipment;
		this->equipment = NULL;
	}
	if (this->stanceComponent)
	{
		delete this->stanceComponent;
		this->stanceComponent = NULL;
	}
	if (this->waterRectangle)
	{
		delete this->waterRectangle;
		this->waterRectangle = NULL;
	}
}

//* Accessors

//Functions
void Character::getHit(std::vector<Entity*>& entities, DamageStatistic& damageStatistic)
{
	statisticComponent->addHealth(-damageStatistic.getDamage());
	entities.push_back(new Effect(
		this->sprite.getPosition(),
		HitEffect,
		freeId(entities)));
}

void Character::setMovementTarget(const sf::Vector2f& target)
{
	MovingEntity::setMovementTarget(target);

	if (this->equipment->getWeapon())
	{
		this->equipment->getWeapon()->resetTarget();
	}
}

void Character::setWeaponTarget(const sf::Vector2f& target)
{
	if (this->equipment->getWeapon()
		&& this->movementComponent->getVelocity() == sf::Vector2f(0.f, 0.f)
		&& (!this->equipment->getWeapon()->getTarget() || *this->equipment->getWeapon()->getTarget() != target))
	{
		this->equipment->getWeapon()->setTarget(target);
	}
}

void Character::addEquipment(EquipmentName equipmentName)
{
	// equipmentDict[equipmentName] stocke toutes les infos liees à l'equipment à ajouter

	// deleting currently equipped equipment
	if (this->equipment->getEquipment()[equipmentDict[equipmentName].equipmentType - 2]) // -2 : cf equipmentType def
		delete this->equipment->getEquipment()[equipmentDict[equipmentName].equipmentType - 2];

	// adding the new equipment
	this->equipment->addEquipment(equipmentDict[equipmentName]);

	// updating the entity's texture
	this->stanceComponent->updateCharacterTexture(this->equipment->getEquipment(), this->equipment->getWeapon());
}

void Character::addWeapon(unsigned short weaponName)
{
	// deleting currently equipped weapon
	if (this->equipment->getWeapon())
		delete this->equipment->getWeapon();
	// adding the new weapon
	this->equipment->addWeapon(weaponName, this->sprite);
	// updating the entity's texture
	this->stanceComponent->updateCharacterTexture(this->equipment->getEquipment(), this->equipment->getWeapon());
}

//* Network
void Character::toDataTransmitted(DataTransmitted& data) const
{
	Entity::toDataTransmitted(data);

	{ // body and equipment type for the character
		data.body[0] = this->hair->getBodyType();
		data.body[1] = this->head->getBodyType();
		data.body[2] = this->body->getBodyType();
		data.body[3] = this->sex;

		if (this->equipment->getWeapon())
			data.equipment[0] = this->equipment->getWeapon()->getEquipmentName();
		else
			data.equipment[0] = (unsigned short)0;

		std::vector<Equipment*> equipment = this->equipment->getEquipment();
		for (size_t i = 0; i < equipment.size(); i++)
		{
			if (equipment[i])
				data.equipment[equipment[i]->getEquipmentType() - 1] = equipment[i]->getEquipmentName();
		}
	}

	{ // state & target
		if (this->equipment->getWeapon() && this->equipment->getWeapon()->getTarget())
		{
			data.state = 1;
			data.target = *(this->equipment->getWeapon()->getTarget());
		}
		else if (this->movementComponent->getTarget())
		{
			data.state = 2;
			data.target = *(this->movementComponent->getTarget());
		}
	}

	{ // statistiques
		data.health = this->statisticComponent->getHealth();
	}
}

void Character::updateAccordingToServerData(DataTransmitted& data)
{
	Entity::updateAccordingToServerData(data);

	{ // body and equipment type for the character
		if (this->equipment->getWeapon()
			&& data.equipment[0] != this->equipment->getWeapon()->getEquipmentName())
		{
			this->addWeapon(data.equipment[0]);
		}

		if (this->equipment->getEquipment()[0]
			&& data.equipment[1] != this->equipment->getEquipment()[0]->getEquipmentName())
		{
			this->addEquipment((EquipmentName)data.equipment[1]);
		}

		if (this->equipment->getEquipment()[1]
			&& data.equipment[2] != this->equipment->getEquipment()[1]->getEquipmentName())
		{
			this->addEquipment((EquipmentName)data.equipment[2]);
		}
	}

	{ // stats
		this->statisticComponent->setHealth(data.health);
	}
}

void Character::updateAccordingToClientData(DataTransmitted& data)
{
	switch (data.state) // state & target
	{
		case 1:
			this->setWeaponTarget(data.target);
			break;
		case 2:
			this->setMovementTarget(data.target);
			break;
	}
}

bool Character::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	this->statisticComponent->update(dt);

	if (this->statisticComponent->getHealth() == 0)
	{
		return 0;
	}

	if (HitboxAndMovingEntity::update(dt, entities, mapEntities, gameMap, visibilityMap)) // update speed and move the entity
	{
		this->updateStance(); // Donne au personnage la texture correspondant a son etat

		this->updateWaterRect(gameMap);

		if (this->equipment && this->equipment->getWeapon())
		{
			this->equipment->getWeapon()->update(dt, entities, this->movementComponent->getVelocity());
		}

		if (visibilityMap && this->isAlliedToPlayer)
		{
			visibilityMap->updateVisibility(this->getCoord(), this->viewRange);
			this->updateFieldOfView(entities, mapEntities);
		}

		return true;
	}

	return false;
}

void Character::render(sf::RenderTarget& target) const
{
	if (this->isInViewRange)
	{
		Entity::render(target);

		if (this->equipment->getWeapon())
		{
			this->equipment->getWeapon()->render(target);
		}

		if (this->waterRectangle)
			target.draw(*this->waterRectangle);

		this->statisticComponent->render(target);
	}

	//if (this->hitboxComponent)
	//	this->hitboxComponent->render(target);
}