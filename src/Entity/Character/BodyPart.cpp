
#include "BodyPart.h"

BodyPart::BodyPart(unsigned short bodyType) :
	bodyType(bodyType)
{
}

BodyPart::~BodyPart()
{
}

void BodyPart::setBodyPart(std::string& name)
{
	this->texture.loadFromFile("Ressources/Images/Sprites/Character/" + name + ".png");
	this->texture.setSmooth(true);

	this->name = name;
}

unsigned short BodyPart::getBodyType() const
{
	return this->bodyType;
}

Hair::Hair(HairType hairType) :
	BodyPart(hairType)
{
	std::string hair = "default";

	switch (hairType)
	{
		case hairM1:
			hair = "HairM1";
			break;

		case hairF1:
			hair = "HairF1";
			break;
	}
	this->setBodyPart(hair);
}
Hair::~Hair()
{}

Head::Head(HeadType headType) :
	BodyPart(headType)
{
	std::string head = "default";
	switch (headType)
	{
		case head1:
			head = "Head1";
			break;
	}
	this->setBodyPart(head);
}
Head::~Head()
{}

Body::Body(BodyType bodyType) :
	BodyPart(bodyType)
{
	std::string body = "default";
	switch (bodyType)
	{
		case bodyM1:
			body = "BodyM1";
			break;
		case bodyF1:
			body = "BodyF1";
			break;
	}

	this->setBodyPart(body);
}
Body::~Body()
{}