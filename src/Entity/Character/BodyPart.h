#pragma once

#include "EntityEnum.h"

class BodyPart
{
protected:
	unsigned short bodyType;

	void setBodyPart(std::string& name);

public:
	sf::Texture texture;
	std::string name;

	unsigned short getBodyType() const;

	BodyPart(unsigned short bodyType);
	~BodyPart();
};

class Hair : public BodyPart
{
public:
	Hair(HairType hairType);
	~Hair();
};

class Head : public BodyPart
{
public:
	Head(HeadType headType);
	~Head();
};

class Body : public BodyPart
{
public:
	Body(BodyType bodyType);
	~Body();
};