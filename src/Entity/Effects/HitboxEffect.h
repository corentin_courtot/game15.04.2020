#pragma once

#include "DamageStatistic.h"
#include "Effect.h"
#include "HitboxEntity.h"

class HitboxEffect : public HitboxEntity, public Effect
{
private:
	int hitboxStance;
	DamageStatistic damageStatistic;

	void initEffect(const HitboxEffectData& hitboxEffectData, float angleInRadian);

public:
	HitboxEffect(const sf::Vector2f& pos,
		float angle,
		const HitboxEffectData& hitboxEffectData,
		int id = 0,
		unsigned short belongId = 0);
	HitboxEffect(const DataTransmitted& data);
	~HitboxEffect();

	virtual void toDataTransmitted(DataTransmitted& data) const;
	virtual void updateAccordingToServerData(DataTransmitted& data);

	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
	virtual void render(sf::RenderTarget& target) const;
};
