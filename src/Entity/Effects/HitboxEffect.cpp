#include "HitboxEffect.h"

void HitboxEffect::initEffect(const HitboxEffectData& hitboxEffectData, float angleInRadian)
{
	Effect::initEffect(hitboxEffectData.effectdata);

	this->damageStatistic.setDamage(hitboxEffectData.damage);
	this->createHitboxComponent(this->sprite,
		hitboxEffectData.hitboxOffset * cos(angleInRadian),
		hitboxEffectData.hitboxOffset * sin(angleInRadian),
		hitboxEffectData.hitboxSize.x,
		hitboxEffectData.hitboxSize.y);
}

HitboxEffect::HitboxEffect(const sf::Vector2f& pos,
	float angleInRadian,
	const HitboxEffectData& hitboxEffectData,
	int id,
	unsigned short belongId) :
	Entity(hitboxEffectData.effectdata.entityType, id, belongId),
	HitboxEntity(hitboxEffectData.effectdata.entityType, id, belongId),
	Effect(pos, hitboxEffectData.effectdata, id, belongId)
{
	if (angleInRadian < -PI * 0.5f || angleInRadian > PI * .5f)
	{
		this->sprite.setScale(1.f, -1.f);
	}

	this->hitboxStance = hitboxEffectData.hitboxStance;

	this->initEffect(hitboxEffectData, angleInRadian);

	this->sprite.setRotation(angleInRadian * 180 / PI);
}

HitboxEffect::HitboxEffect(const DataTransmitted& data) :
	Entity((EntityType)data.objectType, data.id, data.belongId),
	HitboxEntity((EntityType)data.objectType, data.id, data.belongId),
	Effect(data)
{
	this->hitboxStance = data.health;

	if (data.objectType == meleeSlashEffect)
	{
		this->initEffect(MeleeSlashEffect, data.angle);
	}
	else
	{
		std::cout << "(HitboxEffect) This hitbox effect has not been defined. entityType : " << data.objectType << std::endl;
	}
}

HitboxEffect::~HitboxEffect()
{
}

void HitboxEffect::toDataTransmitted(DataTransmitted& data) const
{
	Effect::toDataTransmitted(data);
	data.health = this->hitboxStance;
	data.damage = this->damageStatistic.getDamage();
}

void HitboxEffect::updateAccordingToServerData(DataTransmitted& data)
{
	Effect::updateAccordingToServerData(data);
	this->hitboxStance = data.health;
	this->damageStatistic.setDamage(data.damage);
}

bool HitboxEffect::update(
	const float& dt,
	std::vector<Entity*>& entities,
	std::vector<Entity*>& mapEntities,
	GameMap* gameMap,
	VisibilityMap* visibilityMap)
{
	if (this->currentStance == this->hitboxStance)
	{
		this->hitboxStance = -1; // une seule frame n'inflige des degats

		std::vector<Entity*> hitEntities = this->getEntitiesColliding(entities, mapEntities);

		if (hitEntities.size() > 0)
		// si le projectile est entré en collision avec une entite
		// on fait subir les degats du projectile aux sprites touchés
		{
			// application des effets du projectile aux entites touchees
			for (size_t i = 0; i < hitEntities.size(); ++i)
			{
				hitEntities[i]->getHit(entities, damageStatistic);
			}
		}
	}

	return Effect::update(dt, entities, mapEntities, gameMap, visibilityMap);
}

void HitboxEffect::render(sf::RenderTarget& target) const
{
	Entity::render(target);

	// if (this->hitboxComponent)
	// 	this->hitboxComponent->render(target);
}