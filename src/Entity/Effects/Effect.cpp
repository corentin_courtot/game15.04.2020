#include "Effect.h"

void Effect::createStanceComponent(
	const EffectData effectData,
	const std::vector<std::string>& keysh)
{
	this->stanceComponent = new StanceComponent(this->sprite);
	this->stanceComponent->setupStance(
		"Ressources/Images/Sprites/Effects/" + effectData.fileName + ".png",
		keys,
		effectData.textureSize,
		effectData.stanceWidth);
}

void Effect::initEffect(const EffectData effectData)
{
	this->stanceWidth = effectData.stanceWidth;
	this->textureSize = effectData.textureSize;

	this->renderingPriority = true; // doit etre dessine apres tout le reste : par dessus les personnages

	this->keys.resize(0);
	for (int i = 0; i < floor(this->textureSize.x / this->stanceWidth); ++i)
	{
		this->keys.push_back(std::to_string(i));
	}

	this->sprite.setOrigin(sf::Vector2f(this->stanceWidth / 2, this->textureSize.y / 2));
	this->sprite.setPosition(pos);

	createStanceComponent(effectData, this->keys);

	this->stanceComponent->play(this->keys[this->currentStance]);
}

Effect::Effect(
	const sf::Vector2f& pos,
	const EffectData effectData,
	int id,
	unsigned short belongId) :
	Entity(effectData.entityType, id, belongId),
	StanceEntity(effectData.entityType, id, belongId),
	pos(pos),
	offset(effectData.offset),
	currentStance(0),
	stanceTime(effectData.stanceTime),
	internTime(0)
{
	this->initEffect(effectData);
}

Effect::Effect(const DataTransmitted& data) :
	Entity((EntityType)data.objectType, data.id, data.belongId),
	StanceEntity((EntityType)data.objectType, data.id, data.belongId),
	pos(data.pos),
	offset(data.target),
	currentStance(data.state),
	stanceTime(data.velocity.y),
	internTime(data.velocity.x)
{
	this->sprite.setRotation(data.angle);

	if (entityType == hitEffect)
		this->initEffect(HitEffect);
}

Effect::~Effect()
{
	if (this->stanceComponent)
	{
		delete this->stanceComponent;
		this->stanceComponent = NULL;
	}
}

void Effect::toDataTransmitted(DataTransmitted& data) const
{
	Entity::toDataTransmitted(data);
	data.target = this->offset;
	data.velocity.x = this->internTime;
	data.velocity.y = this->stanceTime;
	data.state = (unsigned short)this->currentStance;
}

void Effect::updateAccordingToServerData(DataTransmitted& data)
{
	Entity::updateAccordingToServerData(data);
	this->offset = data.target;
	this->currentStance = data.state;
	this->internTime = data.velocity.x;
	this->stanceTime = data.velocity.y;
}

bool Effect::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	this->internTime += dt;

	if (this->internTime > this->stanceTime)
	{
		if (this->currentStance >= (int)this->keys.size() - 1)
		{
			return false;
		}

		++this->currentStance;
		this->stanceComponent->play(this->keys[this->currentStance]);

		this->internTime = 0.f;
	}

	//this->setPosition(this->pos + this->offset);

	return true;
}