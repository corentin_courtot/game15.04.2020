#pragma once

#include "StanceEntity.h"

class Effect :
	public StanceEntity
{
private:
protected:
	sf::Vector2f pos; // sprite auquel est attache l'effet
	sf::Vector2f offset;

	std::vector<std::string> keys; // key of the stances. cf stanceComponent
	int currentStance;			   // stores the key's index of the stance that is currently displayed
								   // utilise pour detecter la derniere stance

	float stanceTime; // one stance duration
	float stanceWidth;
	float internTime;

	void createStanceComponent(
		const EffectData effectData,
		const std::vector<std::string>& keys);

	void initEffect(const EffectData effectData);

public:
	Effect(
		const sf::Vector2f& pos,
		const EffectData effectData,
		int id = 0,
		unsigned short belongId = 0);
	Effect(const DataTransmitted& data);
	~Effect();

	virtual void toDataTransmitted(DataTransmitted& data) const;
	virtual void updateAccordingToServerData(DataTransmitted& data);

	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
};
