#include "Tree.h"

Tree::Tree(sf::Vector2i position, uint8_t type, int id, unsigned short belongId) :
	Entity(tree, id, belongId),
	HitboxEntity(tree, id, belongId)
{
	this->sprite.setOrigin(sf::Vector2f(45.f, 105.f));
	this->setPosition(IndTileToVector2f(position));

	int r = myRand::rand();

	sf::Vector2f scaleDistortion = sf::Vector2f((r % 2) * 2.f - 1.f, (float)r / (3.f * myRand::randMax) + 1.f);

	if (type == 0)
	{
		this->initTexture("Trees");
		this->sprite.setScale(scaleDistortion); //random tree size
	}
	else if (type == 1)
	{
		this->initTexture("Tree2");
		this->sprite.setScale(scaleDistortion); //random tree size
	}

	this->createHitboxComponent(this->sprite,
		0.f,
		-10.f * scaleDistortion.y,
		11.f * scaleDistortion.y,
		20.f * scaleDistortion.y,
		0.f);
}

Tree::~Tree()
{}

void Tree::initTexture(std::string fileName)
{
	this->texture.loadFromFile("Ressources/Images/Sprites/Map/" + fileName + ".png");
	this->texture.setSmooth(true);

	this->sprite.setTexture(texture);

	this->textureSize = sf::Vector2f(90.f, 120.f);
}

void Tree::toDataTransmitted(DataTransmitted& data)
{
	Entity::toDataTransmitted(data);
	data.objectType = tree;
	data.id = this->id;
	data.belongId = this->belongId;
	data.pos = this->sprite.getPosition();
	data.angle = this->sprite.getScale().y;
}

void Tree::render(sf::RenderTarget& target) const
{
	this->HitboxEntity::render(target);

	// if (this->hitboxComponent)
	// 	this->hitboxComponent->render(target);
}