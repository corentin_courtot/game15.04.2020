#pragma once
#include "HitboxEntity.h"

class Tree : public HitboxEntity
{
private:
	sf::Texture texture;

public:
	Tree(sf::Vector2i position, uint8_t type, int id = 0, unsigned short belongId = 0);
	~Tree();

	void initTexture(std::string fileName);

	virtual void toDataTransmitted(DataTransmitted& data);

	void render(sf::RenderTarget& target) const;
};
