#pragma once

#include "EntityEnum.h"

//* Projectiles
struct ProjectileData
{
	EntityType entityType;
	std::string fileName;
	float damage;
	float timeLeft;
	float speed;
};

const ProjectileData Arrow = { EntityType::arrow, "Arrow", 40.f, 1.5f, 950.f };

//* Effects
struct EffectData
{
	EntityType entityType;
	std::string fileName;
	float stanceTime;
	float stanceWidth;
	sf::Vector2f offset;
	sf::Vector2f textureSize;
};

const EffectData HitEffect = {
	EntityType::hitEffect,	 // entityType
	"HitEffect",			 // filename
	0.07f,					 // stanceTime
	30.f,					 // stanceWidth
	sf::Vector2f(0.f, -5.f), // offset
	sf::Vector2f(90.f, 60.f) // textureSize
};

struct HitboxEffectData
{
	EffectData effectdata;
	float damage;
	int hitboxStance; // number of the stance which is gonna inflict damage
	sf::Vector2f hitboxSize;
	float hitboxOffset;
};

const HitboxEffectData MeleeSlashEffect = {
	{
		EntityType::meleeSlashEffect, // entityType
		"MeleeSlashEffect",			  // filename
		0.05f,						  // stanceTime
		30.f,						  // stanceWidth
		sf::Vector2f(0.f, 0.f),		  // offset
		sf::Vector2f(90.f, 60.f)	  // textureSize
	},
	50.f,					  // damage
	1,						  // hitboxStance
	sf::Vector2f(20.f, 20.f), // hitboxSize
	10.f					  // hitboxOffset
};