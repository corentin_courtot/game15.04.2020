#include "Mob.h"

void Mob::initVariables()
{
	this->mobAgressiveness = PASSIVE;
	this->aiComponent = NULL;
	this->stanceComponent = NULL;
	this->movementComponent = NULL;
}

void Mob::initComponent(const int AiSeed)
{
}

void Mob::createStanceComponent(const std::string& fileName, const sf::Vector2f& textureSize, const float stanceWidth)
{
	std::vector<std::string> keys;
	keys.push_back("front");
	keys.push_back("facingRight");
	keys.push_back("back");
	keys.push_back("facingLeft");

	this->stanceComponent = new StanceComponent(this->sprite);
	this->stanceComponent->setupStance(
		"Ressources/Images/Sprites/Mobs/" + fileName + ".png",
		keys,
		textureSize,
		stanceWidth);
}

void Mob::createStatisticComponent(const EntityType mobType)
{
	this->statisticComponent = new StatisticComponent(this->sprite);
}

void Mob::createAiComponent(const int AiSeed)
{
	if (this->movementComponent)
	{
		if (this->mobAgressiveness == PASSIVE)
			this->aiComponent = new PassiveAi(this->movementComponent, AiSeed);
	}
	else
	{
		std::cout << "(Mob)Could not create AiComponent" << std::endl;
	}
}

void Mob::getHit(std::vector<Entity*>& entities, DamageStatistic& damageStatistic)
{
	this->statisticComponent->addHealth(-damageStatistic.getDamage());
	entities.push_back(new Effect(
		this->sprite.getPosition(),
		HitEffect,
		freeId(entities)));
}

void Mob::updateStance()
{
	if (!this->stanceComponent)
		std::cout << "ERROR::MOB::No stance defined for this mob : " << this->entityType << std::endl;
	else
	{
		std::string stance = "front";
		switch (this->movementComponent->getState())
		{
			case (IDLE):
				break;

			case (WALKING_DOWN):
				break;

			case (WALKING_LEFT):
				stance = "facingLeft";
				break;

			case (WALKING_RIGHT):
				stance = "facingRight";
				break;

			case (WALKING_UP):
				stance = "back";
				break;

			default:
				break;
		}
		this->stanceComponent->play(stance);
	}
}

/*************************************************
 *				Constructor
 *************************************************/
Mob::Mob(EntityType mobType, int id, unsigned short belongId, const int AiSeed) :
	Entity(mobType, id, belongId),
	HitboxAndMovingEntity(mobType, id, belongId)
{
	this->initVariables();
}

Mob::~Mob()
{
	delete aiComponent;
	delete stanceComponent;
	delete statisticComponent;
}

/************************************************
 * 				Network
 * **********************************************/
void Mob::toDataTransmitted(DataTransmitted& data) const
{
	Entity::toDataTransmitted(data);

	if (this->movementComponent->getTarget())
	{
		data.state = 2;
		data.target = *(this->movementComponent->getTarget());
	}

	if (this->aiComponent)
		data.state = this->aiComponent->getSeed();
}
void Mob::updateAccordingToServerData(DataTransmitted& data)
{
	Entity::updateAccordingToServerData(data);
	if (data.state && this->movementComponent->getTarget())
		this->movementComponent->setMovementTarget(data.target);

	if (this->aiComponent)
		this->aiComponent->setSeed(data.state);
}

/***********************************************
 * 				Rendering
 * *********************************************/
bool Mob::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	if (HitboxAndMovingEntity::update(dt, entities, mapEntities, gameMap, visibilityMap))
		this->updateStance();

	if (this->aiComponent)
	{
		this->aiComponent->update(dt, entities, mapEntities, gameMap, visibilityMap);
		this->aiComponent->randomMovement(this->getCoord(), gameMap);
	}

	this->statisticComponent->update(dt);

	if (this->statisticComponent->getHealth() == 0)
	{
		return 0;
	}

	return true;
}
void Mob::render(sf::RenderTarget& target) const
{
	Entity::render(target);

	//this->statisticComponent->render(target);
}