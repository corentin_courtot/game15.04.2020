#pragma once

#include "AiComponent.h"
#include "MovementComponent.h"

class PassiveAi : public AiComponent
{
private:
public:
	void randomMovement(sf::Vector2f pos, GameMap* gameMap);

	/*			Constructor				*/
	PassiveAi(MovementComponent* movementComponent, const int AiSeed);
	~PassiveAi();

	/*			Update					*/
	//void update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap);
};
