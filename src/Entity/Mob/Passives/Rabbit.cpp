#include "Rabbit.h"

void Rabbit::initComponents(sf::Vector2f pos, const int AiSeed)
{
	this->createMovementComponent(75.f);
	this->createHitboxComponent(this->sprite, 0.f, -2.f, 13.f, 15.f, 0.f);
	this->createStatisticComponent(this->entityType);
	this->createStanceComponent("Rabbit", sf::Vector2f(120.f, 30.f), 30.f);
	this->createAiComponent(AiSeed);
}

void Rabbit::initVariables(sf::Vector2f pos)
{
	this->mobAgressiveness = PASSIVE;
	this->setPosition(IndTileToVector2f(vector2fToIndTile(pos)));
	this->sprite.setOrigin(15.f, 20.f);
}
/*				Constructor				*/
Rabbit::Rabbit(sf::Vector2f pos, int id, unsigned short belongId, const int AiSeed) :
	Entity(rabbit, id, belongId),
	Mob(rabbit, id, belongId, AiSeed)
{
	this->initVariables(pos);
	this->initComponents(pos, AiSeed);
}

Rabbit::Rabbit(DataTransmitted data) :
	Entity((EntityType)data.objectType, data.id, data.belongId),
	Mob((EntityType)data.objectType, data.id, data.belongId, data.state)
{
	this->initVariables(data.pos);
	this->initComponents(data.pos, data.state);
}
Rabbit::~Rabbit()
{
}

bool Rabbit::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
	return Mob::update(dt, entities, mapEntities, gameMap, visibilityMap);
}

void Rabbit::render(sf::RenderTarget& target) const
{
	if (this->isInViewRange)
		Entity::render(target);

	/* if (this->hitboxComponent)
		this->hitboxComponent->render(target); */
}