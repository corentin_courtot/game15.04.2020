#pragma once

#include "Mob.h"

class Rabbit : public Mob
{
private:
	void initComponents(sf::Vector2f pos, const int AiSeed);
	void initVariables(sf::Vector2f pos);

public:
	Rabbit(sf::Vector2f pos, int id, unsigned short belongId, const int AiSeed = 0);
	Rabbit(DataTransmitted data);
	~Rabbit();

	bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
	virtual void render(sf::RenderTarget& target) const;
};
