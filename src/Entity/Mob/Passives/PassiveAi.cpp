#include "PassiveAi.h"

PassiveAi::PassiveAi(MovementComponent* movementComponent, const int AiSeed) :
	AiComponent(movementComponent, AiSeed)
{
}

PassiveAi::~PassiveAi()
{
}

void PassiveAi::randomMovement(sf::Vector2f pos, GameMap* gameMap)
{
	if (this->movementComponent) //&& this->movementState)
	{
		this->movementState = false;
		if (this->movementComponent->getState() == IDLE && (float)this->rand() / this->randMax < 0.01f)
		{
			float randNumber = (float)this->rand() / this->randMax;

			sf::Vector2i indTilePos = vector2fToIndTile(pos);

			if (randNumber < 0.25f)
			{
				if (!gameMap->tiles[indTilePos.x - 1][indTilePos.y].is_occupied())
					this->setMovementTarget(sf::Vector2f(pos.x - tileSize.x, pos.y));
			}
			else if (randNumber < 0.5f)
			{
				if (!gameMap->tiles[indTilePos.x + 1][indTilePos.y].is_occupied())
					this->setMovementTarget(sf::Vector2f(pos.x + tileSize.x, pos.y));
			}
			else if (randNumber < 0.75f)
			{
				if (!gameMap->tiles[indTilePos.x][indTilePos.y - 1].is_occupied())
					this->setMovementTarget(sf::Vector2f(pos.x, pos.y - tileSize.y));
			}
			else
			{
				if (!gameMap->tiles[indTilePos.x][indTilePos.y + 1].is_occupied())
					this->setMovementTarget(sf::Vector2f(pos.x, pos.y + tileSize.y));
			}
		}
	}
}

/* void PassiveAi::update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap)
{
} */