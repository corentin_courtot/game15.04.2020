#pragma once

#include "AiComponent.h"
#include "Effect.h"
#include "HitboxAndMovingEntity.h"
#include "PassiveAi.h"
#include "SelectedEntity.h"
#include "StanceComponent.h"
#include "StatisticComponent.h"

/**
 * TODO:
 *
 * 	Texture (filename + setupStance) ok
 *  Mouvement ok
 * 	IA		ok
 * 	Update	ok
 *  hitUpdate	ok
 * 	Rendering	ok
 * 	Network
 * 	Statistics	ok
 *
 */

enum MobAgressiveness : unsigned short
{
	PASSIVE = 0,
	AGRESSIVE
};

class Mob : public HitboxAndMovingEntity
{
protected:
	SelectedEntity selection; //for debug purposes

	MobAgressiveness mobAgressiveness;

	AiComponent* aiComponent;
	StanceComponent* stanceComponent;
	StatisticComponent* statisticComponent;

	void initVariables();
	void initComponent(const int AiSeed);
	void createStanceComponent(const std::string& fileName, const sf::Vector2f& getTextureSize, const float stanceWidth);
	void createStatisticComponent(const EntityType mobType);
	void createAiComponent(const int AiSeed);

	virtual void getHit(std::vector<Entity*>& entities, DamageStatistic& damageStatistic);

	virtual void updateStance();

public:
	/*		Constructor		*/
	Mob(EntityType mobType, int id, unsigned short belongId, const int AiSeed = 0);
	~Mob();

	/*		Network			*/
	virtual void toDataTransmitted(DataTransmitted& data) const;
	virtual void updateAccordingToServerData(DataTransmitted& data);

	/*		Rendering		*/
	virtual bool update(const float& dt, std::vector<Entity*>& entities, std::vector<Entity*>& mapEntities, GameMap* gameMap, VisibilityMap* visibilityMap = NULL);
	virtual void render(sf::RenderTarget& target) const;
};
